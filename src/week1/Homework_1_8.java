package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 8: Write a program that accepts 3 integers from the user
 * and return true if the 2nd is greater than 1st and 3rd is greater than 2nd.
 * Consider also a condition which, if it is true, 2nd does not need to be > 1st.
 * [CostelRo: below is my interpretation of the meaning of the second phrase
 * so it doesn't contradict the first phrase].
 */


public class Homework_1_8
{
    public static void main(String[] args)
    {
        int a = 11;
        int b = 2;
        int c = 3;
        boolean optionalNumbersRelationship = true; // if true, 2nd number doesn't need to be greater than 1st number.

        boolean SecondGreaterThanFirst = b > a;
        boolean ThirdGreaterThanSecond = c > b;

        String result = ( (optionalNumbersRelationship && ThirdGreaterThanSecond) ||
                            (!optionalNumbersRelationship && SecondGreaterThanFirst && ThirdGreaterThanSecond) ) ?
                        "True" :
                        "False";

        System.out.println(result);
    }
}
