package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 4: Print the area and circumference of a circle from the radius.
 * (learn which types of variables to choose?)
 */


public class Homework_1_4
{
    public static void main(String[] args)
    {
        double radius = 1.6;
        boolean isValidCircle = (radius >= 0);

        double myPi = 3.141592;

        double circleCircumference = 2 * myPi * radius;
        double circleArea = myPi * radius * radius;

        String results = (isValidCircle) ?
                ("Circle circumference = " + circleCircumference +
                        "\n" +
                        "Circle area = " + circleArea) :
                ("Radius data is invalid, must be >= 0");
        System.out.println(results);
    }
}
