package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 6: Write a program that converts the height of a person
 * from centimeters to feet and inches.
 * The printed values should be two integers.
 */


public class Homework_1_6
{
    public static void main(String[] args)
    {
        int heightCm = 178;

        boolean heightCmIsValid = heightCm >= 0;

        int heightInches = (int) (heightCm / 2.54);
        int heightFeet = heightInches / 12;

        String result = (heightCmIsValid) ?
                "Height is = " + heightFeet + " feet " +
                                (heightInches-heightFeet*12) + " inches." :
                "Invalid height in cm (must be >=0).";

        System.out.println(result);
    }
}
