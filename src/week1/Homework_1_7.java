package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 7: Write a program to accept a positive number <=1000
 * and check that the number is even or not.
 * Prints 1 if the number is even or 0 if the number is odd.
 * (check if they did the check)
 */


public class Homework_1_7
{
    public static void main(String[] args)
    {
        int myNumber = 500;

        boolean myNumberIsValid = (myNumber >= 0) && (myNumber <= 1000);

        String evenNumber = (myNumber%2 == 0) ?
                            "1" :
                            "0";

        String result = (myNumberIsValid) ?
                        evenNumber :
                        "Number is not valid (must be >=0 and <=1000).";

        System.out.println(result);
    }
}
