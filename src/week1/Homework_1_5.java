package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 5: Write a program that converts the height of a person from feet & inches
 * (e.g 5 feet 10 inches) to centimeters (~178cm).
 * The value should be integer.
 * To convert a double value to an integer, you can use the cast operator:
 * double d = 33.4;
 * int i = (int)d; // i will be 33;
 */


public class Homework_1_5
{
    public static void main(String[] args)
    {
        int heightInches = 10;
        int heightFeet = 5;

        boolean feetValid = heightFeet >= 0;
        boolean inchesValid = heightInches >=0;

        int feetConvertedToInches = heightFeet * 12;
        int totalHeightInches = feetConvertedToInches + heightInches;

        int heightCentimeters = (int) (totalHeightInches * 2.54);

        String result = ( feetValid && inchesValid ) ?
                "Height in cm = " + heightCentimeters :
                "Invalid values for feet or inches (must be >=0).";

        System.out.println(result);
    }
}
