package week1;

/**
 * Created by CostelRo on 24-May-18.
 */

/*
 * ASSIGNMENT 11:
 * Print the complex solutions of the quadratic equation, not just the real ones.
 */


public class Homework_1_11
{
    public static void main(String[] args)
    {
        double a = 1;
        double b = 2;
        double c = 3;

        double discriminant = b * b - 4 * a * c;

        double solutionFirstPart = (-b) / 2 * a;
        double solutionSecondPart = (discriminant >= 0) ?
                                    Math.sqrt(discriminant) / 2 * a :
                                    Math.sqrt(-discriminant) / 2 * a;

        if (discriminant >= 0)
        {
            System.out.println("Solution 1 = " + (solutionFirstPart + solutionSecondPart));
            System.out.println("Solution 2 = " + (solutionFirstPart - solutionSecondPart));
        }
        else
        {
            System.out.println("Solution 1 = " + solutionFirstPart + " + i " + solutionSecondPart);
            System.out.println("Solution 2 = " + solutionFirstPart + " - i " + solutionSecondPart);
        }
    }
}
