package week1;

/**
 * Created by CostelRo on 21-May-18.
 */

/* ASSIGNMENT 14:
 * Given the frequency of some photons in hertzs (e.g 4.3MHz = 4300000),
 * print the corresponding wavelength of the light.
 * If it Falls in the visible spectrum,
 * print the corresponding color (eg “green”, “red”, etc.).
 */

/*
 * The computations will use data from https://en.wikipedia.org/wiki/Visible_spectrum:
 *   ================================
 *   Color    Wavelength  Frequency
 *   --------------------------------
 *   Violet   380–450 nm  668–789 THz
 *   Blue     450–495 nm  606–668 THz
 *   Green    495–570 nm  526–606 THz
 *   Yellow   570–590 nm  508–526 THz
 *   Orange   590–620 nm  484–508 THz
 *   Red      620–750 nm  400–484 THz
 *   ================================
 *
 * Light's wavelength = speed of light divided by light's frequency.
 *
 * 1 THz (terahertz) = 1,000,000,000,000 Hertz
 * 1 meter = 1,000,000,000 nanometers
 */


public class Homework_1_14
{
    public static void main(String[] args)
    {
        int lightSpeed = 300_000_000; // in meters per second, a constant
        long oneMeterConvertedToNanometers = 1_000_000_000L;
        long lightSpeedInNanometers = lightSpeed * oneMeterConvertedToNanometers;

        long lightFrequency = 400_000_000_000_000L; // per second; source: user input
        long oneTerahertzConvertedToHertz = 1_000_000_000_000L;

        boolean isLightFrequencyValid = (lightFrequency >= 0);
        String resultInvalidData = (!isLightFrequencyValid) ?
                                    "\nInput data is invalid (must be >=0)." :
                                    "";
        System.out.println(resultInvalidData);

        long lightWavelengthNanometers = lightSpeedInNanometers / lightFrequency;

        String resultNotColor = ( isLightFrequencyValid &&
                ( lightWavelengthNanometers < 380 || lightWavelengthNanometers > 750 ) ) ?
                lightWavelengthNanometers + " nm" :
                "";
        System.out.print(resultNotColor);

        String resultRed = ( isLightFrequencyValid &&
                            ( lightWavelengthNanometers >= 620 && lightWavelengthNanometers <= 750 ) ) ?
                            "Red (" + lightWavelengthNanometers + " nm)" :
                            "";
        System.out.print(resultRed);

        String resultOrange = ( isLightFrequencyValid &&
                                ( lightWavelengthNanometers >= 590 && lightWavelengthNanometers < 620 ) ) ?
                                "Orange (" + lightWavelengthNanometers + " nm)" :
                                "";
        System.out.print(resultOrange);

        String resultYellow = ( isLightFrequencyValid &&
                                ( lightWavelengthNanometers >= 570 && lightWavelengthNanometers < 590 ) ) ?
                                "Yellow (" + lightWavelengthNanometers + " nm)" :
                                "";
        System.out.print(resultYellow);

        String resultGreen = ( isLightFrequencyValid &&
                                ( lightWavelengthNanometers >= 495 && lightWavelengthNanometers < 570 ) ) ?
                                "Green (" + lightWavelengthNanometers + " nm)" :
                                "";
        System.out.print(resultGreen);

        String resultBlue = ( isLightFrequencyValid &&
                                ( lightWavelengthNanometers >= 450 && lightWavelengthNanometers < 495 ) ) ?
                                "Blue (" + lightWavelengthNanometers + " nm)" :
                                "";
        System.out.print(resultBlue);

        String resultViolet = ( isLightFrequencyValid &&
                                ( lightWavelengthNanometers >= 380 && lightWavelengthNanometers < 450 ) ) ?
                                "Violet (" + lightWavelengthNanometers + " nm)" :
                                "";
        System.out.println(resultViolet);
    }
}
