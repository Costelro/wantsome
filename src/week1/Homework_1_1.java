package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 1: Given three values representing the angles in a triangle,
 * print if the triangle is right-angled or not.
 * (check if the triangle is a valid one)
 */


public class Homework_1_1
{
    public static void main(String[] args)
    {
        double angleOne = 45;
        double angleTwo = 90;
        double angleThree = 45;

        double sumOfAngles = angleOne + angleTwo + angleThree;

        boolean isRightAngled = (angleOne==90) || (angleTwo==90) || (angleThree==90);

        String result = (sumOfAngles==180 && isRightAngled) ?
                "Triangle is right-angled." :
                "Triangle non-right-angled or invalid.";

        System.out.println(result);
    }
}
