package week1;

/**
 * Created by CostelRo on 21-May-18.
 */

/*
 * ASSIGNMENT 13:
 * Given the position of a knight on a chess board (e.g column = 2, row = 4),
 * print the list of possible squares that he can move to;
 * for the above examples these would be (1, 6), (1, 2), (3, 6), (3, 2), (4, 3), (4, 5).
 */


public class Homework_1_13
{
    public static void main(String[] args)
    {
        int knightStartColumn = 2;
        int knightStartRow = 4;

        boolean isKnightStartColumnValid = (knightStartColumn >= 1) && (knightStartColumn <= 8);
        boolean isKnightStartRowValid = (knightStartRow >= 1) && (knightStartRow <= 8);

        String result0 = (!isKnightStartColumnValid || !isKnightStartRowValid) ?
                        "Start position is impossible in REAL chess." :
                        "";
        System.out.print(result0);


        /* A knight in chess can only make:
         * -- a 2-spaces move up or down or right or left, followed by...
         * -- a 1-space move at 90 degrees (right or left after up/ down, up or down after right/ left).
         *
         * NOTE: Results below will be displayed as "(column, row)", like in the assignment.
         */

        // moving Up, and then (1) Right or (2) Left
        int knightMoveUp = knightStartRow + 2;

        int knightMoveUpRight = knightStartColumn + 1;
        boolean isKnightMoveUpRightValid = (knightMoveUp >= 1) && (knightMoveUp <= 8) &&
                                            (knightMoveUpRight >=1) && (knightMoveUpRight <= 8);
        String result1 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveUpRightValid) ?
                            "(" + knightMoveUpRight + ", " + knightMoveUp + "), " :
                            "";
        System.out.print(result1);

        int knightMoveUpLeft = knightStartColumn - 1;
        boolean isKnightMoveUpLeftValid = (knightMoveUp >= 1) && (knightMoveUp <= 8) &&
                                            (knightMoveUpLeft >=1) && (knightMoveUpLeft <= 8);
        String result2 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveUpLeftValid) ?
                            "(" + knightMoveUpLeft + ", " + knightMoveUp + "), " :
                            "";
        System.out.print(result2);

        // moving Down, and then (3) Right or (4) Left
        int knightMoveDown = knightStartRow - 2;

        int knightMoveDownRight = knightStartColumn + 1;
        boolean isKnightMoveDownRightValid = (knightMoveDown >= 1) && (knightMoveDown <= 8) &&
                                                (knightMoveDownRight >=1) && (knightMoveDownRight <= 8);
        String result3 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveDownRightValid) ?
                            "(" + knightMoveDownRight + ", " + knightMoveDown + "), " :
                            "";
        System.out.print(result3);

        int knightMoveDownLeft = knightStartColumn - 1;
        boolean isKnightMoveDownLeftValid = (knightMoveDown >= 1) && (knightMoveDown <= 8) &&
                                            (knightMoveDownLeft >=1) && (knightMoveDownLeft <= 8);
        String result4 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveDownLeftValid) ?
                            "(" + knightMoveDownLeft + ", " + knightMoveDown + "), " :
                            "";
        System.out.print(result4);

        // moving Right, and then (5) Up or (6) Down
        int knightMoveRight = knightStartColumn + 2;

        int knightMoveRightUp = knightStartRow + 1;
        boolean isKnightMoveRightUpValid = (knightMoveRight >= 1) && (knightMoveRight <= 8) &&
                                            (knightMoveRightUp >=1) && (knightMoveRightUp <= 8);
        String result5 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveRightUpValid) ?
                            "(" + knightMoveRight + ", " + knightMoveRightUp + "), " :
                            "";
        System.out.print(result5);

        int knightMoveRightDown = knightStartRow - 1;
        boolean isKnightMoveRightDownValid = (knightMoveRight >= 1) && (knightMoveRight <= 8) &&
                                                (knightMoveRightDown >=1) && (knightMoveRightDown <= 8);
        String result6 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveRightDownValid) ?
                            "(" + knightMoveRight + ", " + knightMoveRightDown + "), " :
                            "";
        System.out.print(result6);

        // moving Left, and then (7) Up or (8) Down
        int knightMoveLeft = knightStartColumn - 2;

        int knightMoveLeftUp = knightStartRow + 1;
        boolean isKnightMoveLeftUpValid = (knightMoveLeft >= 1) && (knightMoveLeft <= 8) &&
                                            (knightMoveLeftUp >=1) && (knightMoveLeftUp <= 8);
        String result7 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveLeftUpValid) ?
                            "(" + knightMoveLeft + ", " + knightMoveLeftUp + "), " :
                            "";
        System.out.print(result7);

        int knightMoveLeftDown = knightStartRow - 1;
        boolean isKnightMoveLeftDownValid = (knightMoveLeft >= 1) && (knightMoveLeft <= 8) &&
                                            (knightMoveLeftDown >=1) && (knightMoveLeftDown <= 8);
        String result8 = ( (isKnightStartColumnValid && isKnightStartRowValid) && isKnightMoveLeftDownValid) ?
                            "(" + knightMoveLeft + ", " + knightMoveLeftDown + "), " :
                            "";
        System.out.print(result8);
    }
}
