package week1;

/**
 * Created by CostelRo on 23-May-18.
 */

/*
 * ASSIGNMENT 10:
 * Write a program to find the number of integers
 * within the range of 2 numbers received as input
 * and that are divisible by another number (also received as input).
 *      Example:
 *      x=5, y=20 and p=3, the output should be 5
 *      x=6, y=22 and p=2, the output should be 9
 */


public class Homework_1_10
{
    public static void main(String[] args)
    {
        int x_StartLimit = 5;
        int y_EndLimit = 20;
        int p_Divisor = 3;

        int intervalLength = y_EndLimit - x_StartLimit + 1; // must include both end numbers

        int counterDivisibleNumbers = intervalLength / p_Divisor;

        int remainder = intervalLength % p_Divisor;
        if ( calculateRest(x_StartLimit, y_EndLimit, p_Divisor) < remainder )
        {
            counterDivisibleNumbers++;
        }

        boolean isIntervalValid = y_EndLimit >= x_StartLimit;
        String result = (isIntervalValid) ?
                    "" + counterDivisibleNumbers :
                    "Invalid data (make sure that Y >= X).";

        System.out.println(result);
    }


    public static int calculateRest(int startNumber, int endNumber, int divisor)
    {
        int restInFront = -1;
        int restAtBack = -1;

        for (int i=0; i < divisor; i++)
        {
            if ( (startNumber + i) % divisor == 0 )
            {
                restInFront = i;
                break;
            }
        }

        for (int i=0; i < divisor; i++)
        {
            if ( (endNumber - i) % divisor == 0 )
            {
                restAtBack = i;
                break;
            }
        }

        return ( restInFront + restAtBack );
    }
}
