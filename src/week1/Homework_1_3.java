package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 3: Write a program that calculates the number of milliseconds
 * contained in X hours, Y minutes and Z seconds,
 * where X, Y and Z are inputs introduced by the user.
 * (check that Y < 60, Z < 60, and both >= 0)
 */


public class Homework_1_3
{
    public static void main(String[] args)
    {
        int hours = 1;
        int minutes = 1;
        int seconds = 18;

        boolean hoursIsValid = hours >= 0;
        boolean minutesIsValid = (minutes >= 0) && (minutes < 60);
        boolean secondsIsValid = (seconds >= 0) && (seconds < 60);

        int totalTimeInSeconds = hours*60*60 + minutes*60 + seconds;
        long totalTimeInMilliseconds = totalTimeInSeconds * 1000;

        String result = (hoursIsValid && minutesIsValid && secondsIsValid) ?
                "Total time in milliseconds = " + totalTimeInMilliseconds :
                "Invalid data (all must be >=0).";

        System.out.println(result);
    }
}
