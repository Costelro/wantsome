package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/*
 * ASSIGNMENT 2: Given three values representing the length of the edges of a triangle,
 * print if the triangle is right-angled or not
 * (also check if the triangle is a valid one).
 */


public class Homework_1_2
{
    public static void main(String[] args)
    {
        double sideA = 3;
        double sideB = 4;
        double sideC = 5;

        boolean triangleIsValid = (sideA + sideB > sideC) ||
                (sideA + sideC > sideB) ||
                (sideB + sideC > sideA);

        double sideASquared = sideA * sideA;
        double sideBSquared = sideB * sideB;
        double sideCSquared = sideC * sideC;

        double sumSidesABSquared = sideASquared + sideBSquared;
        double sumSidesACSquared = sideASquared + sideCSquared;
        double sumSidesBCSquared = sideBSquared + sideCSquared;

        boolean isRightAngled = (sumSidesABSquared == sideCSquared) ||
                (sumSidesACSquared == sideBSquared) ||
                (sumSidesBCSquared == sideASquared);

        String result = (triangleIsValid && isRightAngled) ?
                "Triangle is right-angled." :
                "Triangle is not right-angled or is invalid.";

        System.out.println(result);
    }
}

