package week1;

/**
 * Created by CostelRo on 5/19/2018.
 */

/* ASSIGNMENT 9:
 * Write a program that accepts 2 integers values as input between 13 and 89
 * and prints true if there is a common digit in both numbers.
 * Example: x=34, y=48, the output should be true.
 */


public class Homework_1_9
{
    public static void main(String[] args)
    {
        int a = 34;
        int b = 48;

        boolean aIsValid = ( a>=13 && a<=89 );
        boolean bIsValid = ( b>=13 && b<=89 );

        int aFirstDigit = a / 10;
        int aSecondDigit = a % 10;
        int bFirstDigit = b / 10;
        int bSecondDigit = b % 10;

        String result = ( aIsValid && bIsValid &&
                          ( (aFirstDigit==bFirstDigit) || (aFirstDigit==bSecondDigit) ||
                            (aSecondDigit==bFirstDigit) || (aSecondDigit==bSecondDigit) )
                        ) ?
                        "True" :
                        "False (or invalid data)";

        System.out.println(result);
    }
}
