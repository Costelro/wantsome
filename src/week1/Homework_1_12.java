package week1;

/**
 * Created by CostelRo on 24-May-18.
 */

/* ASSIGNMENT 12:
 * Print the solutions of the quadratic equation with only 2 decimals.
 * (3.44 instead of 3.438485435…)
 */


public class Homework_1_12
{
    public static void main(String[] args)
    {
        double a = 1;
        double b = 5;
        double c = 3;

        double discriminant = b * b - 4 * a * c;

        double solutionFirstPart = (-b) / 2 * a;
        double solutionSecondPart = (discriminant >= 0) ?
                Math.sqrt(discriminant) / 2 * a :
                Math.sqrt(-discriminant) / 2 * a;

        if (discriminant >= 0)
        {
            System.out.printf("Solution 1 = %.2f\n", solutionFirstPart + solutionSecondPart);
            System.out.printf("Solution 2 = %.2f\n", solutionFirstPart - solutionSecondPart);
        }
        else
        {
            System.out.printf("Solution 1 = %.2f + %.2f i\n", solutionFirstPart, solutionSecondPart);
            System.out.printf("Solution 2 = %.2f - %.2f i\n", solutionFirstPart, solutionSecondPart);
        }
    }
}
