package week8.homework_8_1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by CostelRo on 13.07.2018.
 */


public class WordProcessor
{
    public static void main(String[] args)
    {
        // count words in a file
        System.out.println("Words in file: " + countWordsFromFile("src/week8/homework_8_1/source-text.txt"));

        // process a pre-existent text file
        String sourceFileName      = "src/week8/homework_8_1/source-text.txt";
        String destinationFileName = "src/week8/homework_8_1/destination-text.txt";
        try( BufferedReader br = new BufferedReader( new FileReader(sourceFileName) );
             BufferedWriter bw = new BufferedWriter( new FileWriter(destinationFileName) ) )
        {
            wordWrap3( br, bw ); // to test the alternative methods: replace with "wordWrap(br)" or "wordWrap2(br, bw)"
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }

        // generate a text file with random contents
        String randomContentsFileName = generateRandomTextFile( 15, 20, 8 );
        System.out.println( randomContentsFileName );
    }


    public static long countWordsFromFile(String fileName)
    {
        ArrayList<String> fileContents = getContentsOfTextFile( fileName );

        long result = 0;
        for( String line : fileContents )
        {
            result += countWordsFromString( line );
        }

        return result;
    }


    /**
     * A method that accepts a BufferedReader representing an input file as its parameter
     * and outputs each line of the file to the console,
     * word-wrapping all lines that are longer than 60 characters.
     * For example, if a line contains 112 characters, the method should replace it with two lines:
     * - one containing the first 60 characters
     * - and another containing the final 52 characters.
     * @param source a BufferedReader
     */
    public static void wordWrap(BufferedReader source)
    {
        try
        {
            String currentLine = null;
            while( (currentLine = source.readLine()) != null )
            {
                // some programs, including Microsoft Notepad,
                // add a byte order mark '\uFEFF' at the beginning of UTF-encoded files
                // (which should have been stripped automatically, but Java doesn't do that).
                if (currentLine.startsWith("\uFEFF"))
                {
                    currentLine = String.copyValueOf(currentLine.toCharArray(), 1, currentLine.length()-1);
                }

                int len = currentLine.length();
                int count = currentLine.length() / 60;
                int remainder = currentLine.length() % 60;
                if( len > 60 )
                {
                    for( int i=0; i < count; i++ )
                    {
                        System.out.println( currentLine.substring( i*60, (i+1)*60 ) );
                    }
                    if( remainder > 0 )
                    {
                        System.out.println( currentLine.substring( len - remainder, len ) );
                    }
                }
                else
                {
                    System.out.println( currentLine );
                }
            }
        }
        catch( IOException ioe )
        {
            ioe.printStackTrace();
        }
    }


    /**
     * A method that accepts a second parameter for a BufferedWriter to write the data,
     * and outputs the newly wrapped text to that BufferedWriter rather than to the console.
     * Also, modify it to use a local variable to store the maximum line length rather than hard-coding 60.
     * @param source a BufferedReader
     * @param destination a BufferedWriter
     */
    public static void wordWrap2(BufferedReader source, BufferedWriter destination)
    {
        int maxLineLength = 60;

        try
        {
            String currentLine = null;
            while( (currentLine = source.readLine()) != null )
            {
                // some programs, including Microsoft Notepad,
                // add a byte order mark '\uFEFF' at the beginning of UTF-encoded files
                // (which should have been stripped automatically, but Java doesn't do that).
                if (currentLine.startsWith("\uFEFF"))
                {
                    currentLine = String.copyValueOf(currentLine.toCharArray(), 1, currentLine.length()-1);
                }

                int len = currentLine.length();
                int count = currentLine.length() / maxLineLength;
                int remainder = currentLine.length() % maxLineLength;
                if( len > maxLineLength )
                {
                    for( int i=0; i < count; i++ )
                    {
                        destination.write( currentLine.substring( i*maxLineLength, (i+1)*maxLineLength ) );
                        destination.newLine();
                        destination.flush();
                    }
                    if( remainder > 0 )
                    {
                        destination.write( currentLine.substring( len - remainder, len ) );
                        destination.newLine();
                        destination.flush();
                    }
                }
                else
                {
                    destination.write( currentLine );
                    destination.newLine();
                    destination.flush();
                }
            }
        }
        catch( IOException ioe )
        {
            ioe.printStackTrace();
        }
    }


    /**
     * A method that wraps only whole words, never chopping a word in half.
     * Assume that a word is any whitespace-separated token and that all words are under 60 characters in length.
     * Make sure that each time you wrap a line, the subsequent wrapped line(s) each begin with a word
     * and not with any leading whitespace.
     * @param source a BufferedReader
     * @param destination a BufferedWriter
     */
    public static void wordWrap3(BufferedReader source, BufferedWriter destination)
    {
        int maxLineLength = 60;

        try
        {
            String currentLine = null;
            StringBuilder temp = null;
            while( (currentLine = source.readLine()) != null )
            {
                temp = new StringBuilder("");
                // some programs, including Microsoft Notepad,
                // add a byte order mark '\uFEFF' at the beginning of UTF-encoded files
                // (which should have been stripped automatically, but Java doesn't do that).
                if (currentLine.startsWith("\uFEFF"))
                {
                    currentLine = String.copyValueOf(currentLine.toCharArray(), 1, currentLine.length()-1);
                }

                String[] sourceAsArray = currentLine.split("\\s+");
                for( int i=0; i < sourceAsArray.length; i++ )
                {
                    String word = sourceAsArray[i];
                    if( i == 0 )
                    {
                        temp.append(word);
                    }
                    else if( (temp.length() + word.length() + 1) <= maxLineLength )
                    {
                        temp.append(" ").append(word);
                    }
                    else
                    {
                        destination.write( temp.toString() );
                        destination.newLine();
                        destination.flush();

                        temp = new StringBuilder("");
                        temp.append(word);
                    }
                }

                if( temp.length() > 0 )
                {
                    destination.write( temp.toString() );
                    destination.newLine();
                    destination.flush();
                }
            }
        }
        catch( IOException ioe )
        {
            ioe.printStackTrace();
        }
    }


    /**
     * This methods counts the words from a string.
     * A 'word' is a sequence of non-whitespace characters (as defined by Java).
     * @param source a string
     * @return an int the number of words in the source string
     */
    public static int countWordsFromString(String source)
    {
        String[] splitString = null;
        if (source != null)
        {
            splitString = source.split("\\s+");
            return splitString.length;
        }
        else
        {
            return 0;
        }
    }


    private static ArrayList<String> getContentsOfTextFile(String fileName)
    {
        ArrayList<String> result = new ArrayList<>();

        try( BufferedReader br = new BufferedReader( new FileReader( fileName ) ) )
        {
            String currentLine = null;
            while( (currentLine = br.readLine()) != null )
            {
                result.add(currentLine);
            }
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }

        return result;
    }


    /**
     * This method generates input text files for the above 'wordWrap' methods
     * by randomly generating characters and spaces to create words.
     *
     * @param maxLineCount
     *        the maximum number of lines of text in the generated file
     *
     * @param maxLineLength
     *        the maximum number of words (characters delimited by whitespaces) in each line of text in the file
     *
     * @param maxWordLength
     *        the maximum number of characters in each word
     *
     * @return String
     *         the path to the generated file
     */
    private static String generateRandomTextFile( int maxLineCount, int maxLineLength, int maxWordLength )
    {
        String filePath = "src/week8/homework_8_1/";
        String fileName = "source-random-text.txt";

        try( BufferedWriter bw = new BufferedWriter( new FileWriter( filePath + fileName ) ) )
        {
            for ( int i = 0; i < maxLineCount; i++ )
            {
                StringBuilder sb = new StringBuilder();
                Random rand = new Random();
                int currentLineLength = rand.nextInt(maxLineLength) + 1;
                for ( int j = 0; j < currentLineLength - 1; j++ )
                {
                    sb.append( generateRandomWord(maxWordLength) ).append( " " );
                }

                // add the end of the sentence
                sb.append( generateRandomWord(maxWordLength) ).append(". ").append( System.lineSeparator() );

                bw.write( sb.toString() );
            }

            // write sentence to file
            bw.flush();
        }
        catch( IOException ioe )
        {
            ioe.printStackTrace();
        }

        return filePath + fileName;
    }


    private static String generateRandomWord(int maxWordLength)
    {
        Random rand = new Random();
        int wordLength = rand.nextInt(maxWordLength-2) + 3; // avoid words of 0-2 characters
        char[] word = new char[wordLength];
        for( int i=0; i < wordLength; i++ )
        {
            word[i] = (char) ('a' + rand.nextInt(26));
        }

        return new String( word );
    }
}
