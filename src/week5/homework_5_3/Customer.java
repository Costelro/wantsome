package week5.homework_5_3;

import java.util.Objects;

/**
 * Created by CostelRo on 21.06.2018.
 */


class Customer
{
    // fields
    private String clientName;
    private MembershipTypes membershipType;
    private Discount clientDiscounts;


    // constructors
    Customer(String name)
    {
        this.clientName = name;
        this.membershipType = MembershipTypes.NONE;
        this.clientDiscounts = new Discount( this.membershipType );
    }

    Customer(String name, MembershipTypes membership)
    {
        this(name);
        this.membershipType = membership;
        this.clientDiscounts = new Discount( membership );
    }


    // getters and setters
    public String getClientName()
    {
        return this.clientName;
    }

    public void setClientName(String newClientName)
    {
        if ( !Objects.equals(newClientName, null) && newClientName.length() > 0 )
        {
            this.clientName = newClientName;
        }
    }

    public Discount getClientDiscounts()
    {
        return this.clientDiscounts;
    }

    public MembershipTypes getMembershipType()
    {
        return this.membershipType;
    }

    public void setMembershipType(MembershipTypes newMembershipType)
    {
        this.membershipType = newMembershipType;
    }


    // other methods
    @Override
    public String toString()
    {
        return ( this.clientName + " (client " + this.membershipType.toString() + ")");
    }
}
