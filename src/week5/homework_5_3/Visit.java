package week5.homework_5_3;

import java.time.LocalDate;

/**
 * Created by CostelRo on 21.06.2018.
 */


class Visit
{
    // fields
    private static String    currency = "RON";
    private        LocalDate date;
    private        Customer  customer;
    private        double    servicesTotalValue;
    private        double    productsTotalValue;


    // constructors
    Visit(LocalDate date, Customer customer)
    {
        this.date = date;
        this.customer = customer;
        this.servicesTotalValue = 0;
        this.productsTotalValue = 0;
    }

    Visit(LocalDate date, Customer customer, double services, double products)
    {
        this(date, customer);

        if (services > 0) { this.servicesTotalValue = services; }
        else { this.servicesTotalValue = 0; }

        if (products > 0) { this.productsTotalValue = products; }
        else { this.productsTotalValue = 0; }
    }


    // getters and setters
    public LocalDate getDate()
    {
        return this.date;
    }

    public Customer getCustomer()
    {
        return this.customer;
    }

    public void setCustomer(Customer newCustomer)
    {
        if (newCustomer != null) { this.customer = newCustomer; }
    }

    public double getServicesTotalValue()
    {
        return this.servicesTotalValue;
    }

    public String setServicesTotalValue(double newServicesTotalValue)
    {
        if (newServicesTotalValue > 0)
        {
            this.servicesTotalValue = newServicesTotalValue;
            return "ok";
        }
        else
        {
            this.servicesTotalValue = 0;
            return "Services total value must be >= 0.";
        }
    }

    public double getProductsTotalValue()
    {
        return this.productsTotalValue;
    }

    public String setProductsTotalValue(double newProductsTotalValue)
    {
        if (newProductsTotalValue > 0)
        {
            this.productsTotalValue = newProductsTotalValue;
            return "ok";
        }
        else
        {
            this.productsTotalValue = 0;
            return "Products total value must be >= 0.";
        }
    }


    // other methods
    public double getTotalPurchaseBeforeDiscounts()
    {
        return this.getServicesTotalValue() + this.getProductsTotalValue();
    }


    public double getTotalPurchaseAfterDiscounts()
    {
        double valueOfServicesAfterDiscount = this.getServicesTotalValue()
                                              * ( 1 - this.customer.getClientDiscounts().getServiceDiscountLevel() );
        double valueOfProductsAfterDiscount = this.getProductsTotalValue()
                                              * ( 1 - this.customer.getClientDiscounts().getProductDiscountLevel() );

        return (valueOfServicesAfterDiscount + valueOfProductsAfterDiscount);
    }


    @Override
    public String toString()
    {
        return "Date: " + this.getDate() + "\n" + this.customer.getClientName().toUpperCase() + "\n"
               + getTotalPurchaseAfterDiscounts() + " " + Visit.currency
               + " (" + getTotalPurchaseBeforeDiscounts() + " " + Visit.currency + " before discounts)";
    }
}
