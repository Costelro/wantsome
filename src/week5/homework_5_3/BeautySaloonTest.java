package week5.homework_5_3;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;


/**
 * Created by CostelRo on 21.06.2018.
 */

/*
 * ASSIGNMENT 5_3:
 * Write a discount system for a beauty saloon, which provides services and sells beauty products.
 * It offers 3 types of memberships: Premium, Gold and Silver.
 * Premium, gold and silver members receive a discount of 20%, 15%, and 10%, respectively, for all services provided.
 * Customers without membership receive no discount.
 * All members receives a flat 10% discount on products purchased (this might change in future).
 * Your system shall consist of three classes: Customer, Discount and Visit, as shown in the class diagram.
 * It shall compute the total bill if a customer purchases $x of products and $y of services, for a visit.
 * Also write a test program to exercise all the classes.
 */


public class BeautySaloonTest
{
    @Test
    public void createACustomerWithoutMembershipType()
    {
        Customer testCustomer1 = new Customer("Cleopa");

        assertEquals(MembershipTypes.NONE, testCustomer1.getMembershipType());
        assertEquals("Cleopa", testCustomer1.getClientName());
        assertEquals(0 , testCustomer1.getClientDiscounts().getServiceDiscountLevel(), 5);
        assertEquals(0, testCustomer1.getClientDiscounts().getProductDiscountLevel(), 5);
    }


    @Test
    public void createACustomerWithAllData()
    {
        Customer testCustomer2 = new Customer("Marcela", MembershipTypes.SILVER);
        Customer testCustomer3 = new Customer("Cornelia", MembershipTypes.GOLD);
        Customer testCustomer4 = new Customer("Ingrid", MembershipTypes.PREMIUM);

        assertEquals(MembershipTypes.SILVER, testCustomer2.getMembershipType());
        assertEquals("Marcela", testCustomer2.getClientName());
        assertEquals(0.1 , testCustomer2.getClientDiscounts().getServiceDiscountLevel(), 5);
        assertEquals(0.1, testCustomer2.getClientDiscounts().getProductDiscountLevel(), 5);

        assertEquals(MembershipTypes.GOLD, testCustomer3.getMembershipType());
        assertEquals("Cornelia", testCustomer3.getClientName());
        assertEquals(0.15 , testCustomer3.getClientDiscounts().getServiceDiscountLevel(), 5);
        assertEquals(0.1, testCustomer3.getClientDiscounts().getProductDiscountLevel(), 5);

        assertEquals(MembershipTypes.PREMIUM, testCustomer4.getMembershipType());
        assertEquals("Ingrid", testCustomer4.getClientName());
        assertEquals(0.2 , testCustomer4.getClientDiscounts().getServiceDiscountLevel(), 5);
        assertEquals(0.1, testCustomer4.getClientDiscounts().getProductDiscountLevel(), 5);
    }


    @Test
    public void displayACustomer()
    {
        Customer testCustomer = new Customer("Marcela", MembershipTypes.SILVER);

        assertEquals("Marcela (client SILVER)", testCustomer.toString());
    }


    @Test
    public void computeTotalPurchaseBeforeDiscountsForPositiveValues()
    {
        Customer testCustomer3 = new Customer("Cornelia", MembershipTypes.GOLD);
        LocalDate date = LocalDate.of(2018, 6, 21);
        Visit testVisit = new Visit(date, testCustomer3);
        testVisit.setServicesTotalValue(100);
        testVisit.setProductsTotalValue(100);

        assertEquals("ok", testVisit.setServicesTotalValue(100));
        assertEquals("ok", testVisit.setProductsTotalValue(100));
        assertEquals(200, testVisit.getTotalPurchaseBeforeDiscounts(), 5);
    }


    @Test
    public void replaceNegativeValuesWithZeroWhenComputingTotalPurchaseBeforeDiscounts()
    {
        Customer testCustomer3 = new Customer("Cornelia", MembershipTypes.GOLD);
        LocalDate date = LocalDate.of(2018, 6, 21);
        Visit testVisit = new Visit(date, testCustomer3);

        assertEquals("Services total value must be >= 0.", testVisit.setServicesTotalValue(-100));
        assertEquals("Products total value must be >= 0.", testVisit.setProductsTotalValue(-100));
        assertEquals(0, testVisit.getTotalPurchaseBeforeDiscounts(), 5);

        assertEquals("ok", testVisit.setServicesTotalValue(100));
        assertEquals("Products total value must be >= 0.", testVisit.setProductsTotalValue(-100));
        assertEquals(100, testVisit.getTotalPurchaseBeforeDiscounts(), 5);

        assertEquals("Services total value must be >= 0.", testVisit.setServicesTotalValue(-100));
        assertEquals("ok", testVisit.setProductsTotalValue(100));
        assertEquals(100, testVisit.getTotalPurchaseBeforeDiscounts(), 5);
    }


    @Test
    public void computeTotalPurchaseAfterDiscountsForPositiveValues()
    {
        Customer testCustomer4 = new Customer("Ingrid", MembershipTypes.PREMIUM);
        LocalDate date = LocalDate.of(2018, 6, 22);
        Visit testVisit = new Visit(date, testCustomer4);
        testVisit.setServicesTotalValue(100);
        testVisit.setProductsTotalValue(100);

        assertEquals("ok", testVisit.setServicesTotalValue(100));
        assertEquals("ok", testVisit.setProductsTotalValue(100));
        assertEquals(170, testVisit.getTotalPurchaseAfterDiscounts(), 5);
    }


    @Test
    public void replaceNegativeValuesWithZeroWhenComputingTotalPurchaseAfterDiscounts()
    {
        Customer testCustomer4 = new Customer("Ingrid", MembershipTypes.PREMIUM);
        LocalDate date = LocalDate.of(2018, 6, 22);
        Visit testVisit = new Visit(date, testCustomer4);

        assertEquals("Services total value must be >= 0.", testVisit.setServicesTotalValue(-100));
        assertEquals("Products total value must be >= 0.", testVisit.setProductsTotalValue(-100));
        assertEquals(0, testVisit.getTotalPurchaseAfterDiscounts(), 5);

        assertEquals("ok", testVisit.setServicesTotalValue(100));
        assertEquals("Products total value must be >= 0.", testVisit.setProductsTotalValue(-100));
        assertEquals(85, testVisit.getTotalPurchaseAfterDiscounts(), 5);

        assertEquals("Services total value must be >= 0.", testVisit.setServicesTotalValue(-100));
        assertEquals("ok", testVisit.setProductsTotalValue(100));
        assertEquals(90, testVisit.getTotalPurchaseAfterDiscounts(), 5);
    }


    @Test
    public void displayAVisit()
    {
        Customer testCustomer1 = new Customer("Cleopa", MembershipTypes.NONE);
        LocalDate date = LocalDate.of(2018, 6, 23);
        Visit testVisit = new Visit(date, testCustomer1);
        testVisit.setServicesTotalValue(100);
        testVisit.setProductsTotalValue(100);

        assertEquals( "Date: 2018-06-23\nCLEOPA\n200.0 RON (200.0 RON before discounts)"
                     , testVisit.toString() );
    }
}
