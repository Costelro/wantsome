package week5.homework_5_3;

/**
 * Created by CostelRo on 21.06.2018.
 */


public enum MembershipTypes
{
    NONE,
    SILVER,
    GOLD,
    PREMIUM
}
