package week5.homework_5_3;

/**
 * Created by CostelRo on 21.06.2018.
 */


class Discount
{
    // fields
    private double serviceDiscountLevel;
    private double productDiscountLevel;


    // getters and setters
    public double getServiceDiscountLevel()
    {
        return this.serviceDiscountLevel;
    }

    public double getProductDiscountLevel()
    {
        return this.productDiscountLevel;
    }


    // constructors
    Discount(MembershipTypes membership)
    {
        switch (membership)
        {
            case NONE:
                this.serviceDiscountLevel = 0;
                this.productDiscountLevel = 0;
                break;
            case SILVER:
                this.serviceDiscountLevel = 0.1;
                this.productDiscountLevel = 0.1;
                break;
            case GOLD:
                this.serviceDiscountLevel = 0.15;
                this.productDiscountLevel = 0.1;
                break;
            case PREMIUM:
                this.serviceDiscountLevel = 0.2;
                this.productDiscountLevel = 0.1;
                break;
        }
    }
}
