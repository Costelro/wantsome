package week5.homework_5_1;

import java.util.Objects;

/**
 * Created by CostelRo on 16-Jun-18.
 */


class Building
{
    // fields
    private String name;
    private BuildingCategory category;
    private double price;
    private String neighborhood;


    // constructors
    public Building(String name, BuildingCategory category, double price, String neighborhood)
    {
        this.name = name;
        this.category = category;
        this.price = price;
        this.neighborhood = neighborhood;
    }


    // getters and setters
    public String getName()
    {
        return this.name;
    }

    public void setName(String newName)
    {
        if ( !Objects.equals(newName, null) && !newName.equals("") )
        {
            this.name = newName;
        }
    }

    public BuildingCategory getCategory()
    {
        return this.category;
    }

    public void setCategory(BuildingCategory newCategory)
    {
        this.category = newCategory;
    }

    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double newPrice)
    {
        if (newPrice >= 0) { this.price = newPrice; }
    }

    public String getNeighborhood()
    {
        return this.neighborhood;
    }

    public void setNeighborhood(String newNeighborhood)
    {
        if ( !Objects.equals(newNeighborhood, null) && !newNeighborhood.equals("") )
        {
            this.neighborhood = newNeighborhood;
        }
    }


    // other methods
    @Override
    public String toString()
    {
        return ( "Building: " + this.name + " (" + this.category + ")" + "\t= " + this.price
                 + "- " + this.neighborhood );
    }
}
