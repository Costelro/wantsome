package week5.homework_5_1;

/**
 * Created by CostelRo on 16-Jun-18.
 */


public enum BuildingCategory
{
    RESIDENTIAL,
    OFFICE,
    HOSPITAL,
    RELIGIOUS
}
