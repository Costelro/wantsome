package week5.homework_5_1;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by CostelRo on 16-Jun-18.
 */

/*
 * ASSIGNMENT 5.1:
 * Create a class representing a building and test it.
 * Note: Implements all entities according to the requirements file.
 */


public class Homework_5_1
{
    public static void main(String[] args)
    {
        // preparations for tests
        BuildingsCatalog testCatalog = new BuildingsCatalog();

        Building b1 = new Building("Spitalul Parhon",
                                    BuildingCategory.HOSPITAL, 5000000, "Copou");
        Building b2 = new Building("Habitat 1",
                                    BuildingCategory.OFFICE, 2000000, "Copou");
        Building b3 = new Building("Centris",
                                    BuildingCategory.RESIDENTIAL, 2400000, "Centru");
        Building b4 = new Building("Habitat 2",
                                    BuildingCategory.OFFICE, 1000000, "Copou");
        Building b5 = new Building("Spitalul Sf. Spiridon",
                                    BuildingCategory.HOSPITAL, 5000000, "Centru");

        testCatalog.addBuilding(b1);
        testCatalog.addBuilding(b2);
        testCatalog.addBuilding(b3);
        testCatalog.addBuilding(b4);
        testCatalog.addBuilding(b5);


        // test 1
        for ( BuildingCategory category : BuildingCategory.values() )
        {
            double average = testCatalog.computeAveragePricePerCategory( category );
            String result = ( average == -1 )
                            ? "No buildings recorded yet."
                            : ( category + " average price = " + average );
            System.out.println(result);
        }

        // test 2
        System.out.println();

        Set<String> listOfNeighborhoods = testCatalog.getListOfNeighborhoods();
        for (String neigborhood: listOfNeighborhoods)
        {
            double average = testCatalog.computeAveragePricePerNeighborhood( neigborhood );
            String result = ( average == -1 )
                            ? "No buildings recorded yet."
                            : ( "\"" + neigborhood + "\" average price = " + average );
            System.out.println(result);
        }

        // test 3
        System.out.printf( "\nDefined categories: %d", BuildingCategory.values().length );

        Set<String> usedCategories = new HashSet<String>(10);
        for ( Building b : testCatalog.getBuildingsCatalog() )
        {
            usedCategories.add( b.getCategory().name() );
        }
        System.out.printf( "\nUsed categories: %d", usedCategories.size() );

        // test 4
        System.out.println();

        Set<String> uniqueNeighborhoods = new HashSet<>(10);
        for (Building b : testCatalog.getBuildingsCatalog())
        {
            uniqueNeighborhoods.add( b.getNeighborhood() );
        }
        System.out.printf( "\nUnique neighborhoods, y'all: %d !\n\n", uniqueNeighborhoods.size() );
    }
}
