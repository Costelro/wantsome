package week5.homework_5_1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by CostelRo on 16-Jun-18.
 */


public class BuildingsCatalog
{
    // fields
    private ArrayList<Building> buildingsCatalog;


    // constructors
    BuildingsCatalog()
    {
        this.buildingsCatalog = new ArrayList<>(10);
    }


    // getters and setters (where needed)
    ArrayList<Building> getBuildingsCatalog()
    {
        return this.buildingsCatalog;
    }


    // other methods
    void addBuilding(Building newBuilding)
    {
        if ( !Objects.equals(newBuilding, null) )
        {
            this.buildingsCatalog.add(newBuilding);
        }
    }


    void removeBuilding(Building building)
    {
        this.buildingsCatalog.remove( building );
    }


    Set<String> getListOfNeighborhoods()
    {
        Set<String> result = new HashSet<>(10);

        for (Building b : this.buildingsCatalog)
        {
            result.add( b.getNeighborhood() );
        }

        return result;
    }


    double computeAveragePricePerCategory(BuildingCategory category)
    {
        if ( (this.buildingsCatalog == null) || (this.buildingsCatalog.size() == 0) ) { return -1; }

        double counter = 0;
        double totalPrice = 0;

        for (Building b : this.buildingsCatalog)
        {
            if (b.getCategory() == category)
            {
                counter++;
                totalPrice += b.getPrice();
            }
        }

        if (counter == 0) { return 0; }
        else { return totalPrice / counter; }
    }


    double computeAveragePricePerNeighborhood(String neighborhood)
    {
        if ( (this.buildingsCatalog == null) || (this.buildingsCatalog.size() == 0) ) { return -1; }

        double counter = 0;
        double totalPrice = 0;

        for (Building b : this.buildingsCatalog)
        {
            if ( b.getNeighborhood().equals(neighborhood) )
            {
                counter++;
                totalPrice += b.getPrice();
            }
        }

        if (counter == 0) { return 0; }
        else { return totalPrice / counter; }
    }
}
