package week5.homework_5_2;

import java.time.LocalDate;

/**
 * Created by CostelRo on 16-Jun-18.
 */


public class StockUpdate implements Comparable
{
    // fields
    private String code;
    private double price;
    private LocalDate date;


    // constructors
    public StockUpdate() {}


    public StockUpdate(String code, double price, LocalDate date)
    {
        this.code = code;
        this.price = price;
        this.date = date;
    }


    // getters and setters
    public String getCode()
    {
        return this.code;
    }

    public void setCode(String newCode)
    {
        // in real life: 'newCode' should be controlled, e.g. only from an enum
        this.code = newCode;
    }

    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double newPrice)
    {
        this.price = newPrice;
    }

    public LocalDate getDate()
    {
        return this.date;
    }

    public void setDate(LocalDate newDate)
    {
        this.date = newDate;
    }

    // other methods
    @Override
    public String toString()
    {
        return ( this.date + ":  " + this.code + " " + this.price );
    }


    @Override
    public int compareTo(Object o)
    {
        int compareDates = this.date.compareTo( ((StockUpdate) o).date );
        int compareCode = this.code.compareTo( ((StockUpdate) o).code );

        if (compareDates == 0) { return compareCode; }
        else if (compareDates < 0){ return -1; }
        else { return 1; }
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockUpdate that = (StockUpdate) o;

        if (Double.compare(that.price, price) != 0) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }


    @Override
    public int hashCode()
    {
        int result;
        long temp;
        result = code != null ? code.hashCode() : 0;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
