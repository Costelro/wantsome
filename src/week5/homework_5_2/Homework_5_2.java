package week5.homework_5_2;

import java.time.LocalDate;

/**
 * Created by CostelRo on 16-Jun-18.
 */

/*
 * ASSIGNMENT 5_2:
 * Create a class called StockMarket that can record and query stock updates.
 * Note: Implement all entities according to the requirements file.
 */


public class Homework_5_2
{
    public static void main(String[] args)
    {
        // preparations for tests
        StockMarket testMarket = new StockMarket();

        LocalDate d1 = LocalDate.of(2018, 6,11);
        StockUpdate u1 = new StockUpdate("AMZN", 200, d1);
        StockUpdate u2 = new StockUpdate("MSFT", 300, d1);
        StockUpdate u3 = new StockUpdate("GOOG", 410, d1);

        LocalDate d2 = d1.plusDays(1);
        StockUpdate u4 = new StockUpdate("AMZN", 220, d2);
        StockUpdate u5 = new StockUpdate("MSFT", 314, d2);
        StockUpdate u6 = new StockUpdate("GOOG", 417, d2);

        LocalDate d3 = d2.plusDays(1);
        StockUpdate u7 = new StockUpdate("AMZN", 205, d3);
        StockUpdate u8 = new StockUpdate("MSFT", 298, d3);
        StockUpdate u9 = new StockUpdate("GOOG", 408, d3);

        testMarket.addStockUpdate(u1);
        testMarket.addStockUpdate(u2);
        testMarket.addStockUpdate(u3);
        testMarket.addStockUpdate(u4);
        testMarket.addStockUpdate(u5);
        testMarket.addStockUpdate(u6);
        testMarket.addStockUpdate(u7);
        testMarket.addStockUpdate(u8);
        testMarket.addStockUpdate(u9);


        // test 1
        for ( StockUpdate su : testMarket.getStockUpdates() )
        {
            System.out.println( su );
        }
        System.out.println();

        // test 2
        LocalDate fromDate1 = LocalDate.of(2018, 6, 12);
        LocalDate toDate1 = LocalDate.of(2018, 6, 12);
        for ( StockUpdate su : testMarket.queryUpdates(fromDate1, toDate1) )
        {
            System.out.println( su.toString() );
        }
        System.out.println();

        // test 3
        LocalDate fromDate2 = LocalDate.of(2018, 5, 5);
        LocalDate toDate2 = LocalDate.of(2018, 6, 12);
        for ( StockUpdate su : testMarket.queryUpdates(fromDate2, toDate2, "MSFT") )
        {
            System.out.println( su );
        }
        System.out.println();

        // test 4
        LocalDate testDate1 = LocalDate.of(2018, 6, 12);
        System.out.println(testMarket.getPrice(testDate1, "GOOG"));
        System.out.println();

        // test 5
        LocalDate testDate2 = LocalDate.of(2018, 6, 13);
        System.out.println(testMarket.getPrices(testDate2));
        System.out.println();
    }
}
