package week5.homework_5_2;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by CostelRo on 16-Jun-18.
 */


public class StockMarket
{
    // fields
    private TreeSet<StockUpdate> stockUpdates = new TreeSet<>();


    // constructors
    public StockMarket()
    {
        this.stockUpdates = new TreeSet<>();
    }


    public StockMarket(TreeSet<StockUpdate> stockUpdates)
    {
        this.stockUpdates = stockUpdates;
    }


    // getters and setters
    Set<StockUpdate> getStockUpdates()
    {
        return this.stockUpdates;
    }


    // other methods
    public void addStockUpdate(StockUpdate update)
    {
        if (update != null) { stockUpdates.add( update ); }
    }


    public Set<StockUpdate> queryUpdates(LocalDate from, LocalDate to)
    {
        if (from.isAfter(to)) { return new TreeSet<>(); }

        Set<StockUpdate> result = new TreeSet<>();

        for (StockUpdate su : this.stockUpdates)
        {
            if ( (su.getDate().isAfter(from) || su.getDate().equals(from))
                 && (su.getDate().isBefore(to) || su.getDate().equals(to)) )
            {
                result.add( su );
            }
        }

        return result ;
    }


    public Set<StockUpdate> queryUpdates(LocalDate from, LocalDate to, String stockCode)
    {
        if (from.isAfter(to)) { return new TreeSet<>(); }

        Set<StockUpdate> result = new TreeSet<>();

        for (StockUpdate su : this.stockUpdates)
        {
            if ( (su.getDate().isAfter(from) || su.getDate().equals(from))
                 && (su.getDate().isBefore(to) || su.getDate().equals(to))
                 && su.getCode().equals(stockCode) )
            {
                result.add( su );
            }
        }

        return result ;
    }


    public double getPrice(LocalDate date, String code)
    {
        StockUpdate result = new StockUpdate();

        long previousTimeDifference = this.stockUpdates.first().getDate().until( date, ChronoUnit.DAYS );

        for (StockUpdate su : this.stockUpdates)
        {
            if ( su.getDate().isBefore(date) || su.getDate().equals(date) )
            {
                long currentTimeDifference = su.getDate().until( date, ChronoUnit.DAYS );
                if ( currentTimeDifference <= previousTimeDifference
                     && su.getCode().equals(code) )
                {
                    previousTimeDifference = currentTimeDifference;
                    result = su;
                }
            }
        }

        return result.getPrice();
    }


    public Map<String, Double> getPrices(LocalDate date)
    {
        TreeMap<String, Double> result = new TreeMap<>();

        long previousTimeDifference = this.stockUpdates.first().getDate().until( date, ChronoUnit.DAYS );

        for (StockUpdate su : this.stockUpdates)
        {
            if ( su.getDate().isBefore(date) || su.getDate().equals(date) )
            {
                long currentTimeDifference = su.getDate().until( date, ChronoUnit.DAYS );
                if ( currentTimeDifference <= previousTimeDifference )
                {
                    previousTimeDifference = currentTimeDifference;
                    result.put( su.getCode(), su.getPrice() );
                }
            }
        }

        return result;
    }
}
