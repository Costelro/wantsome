package week2;

/**
 * Created by CostelRo on 01-Jun-18.
 */

/*
 * ASSIGNMENT 7:
 * Write a function that checks whether an array is sorted or not.
 */


public class Homework_2_7
{
    public static void main(String[] args)
    {
        int[] sourceArray = {9, 8, 7, 6, 5};
        String sortType = "decreasing";  // "increasing" (1..9) / "decreasing" (9..1)

        System.out.printf( "\n%s\n\n", isArraySorted(sourceArray, sortType) );
    }

    private static String isArraySorted(int[] source, String sortType)
    {
        String result = "Array is sorted (" + sortType + ").";


        if (source == null)
        {
            return "Please use a non-null array.";
        }
        else if (source.length <= 1)
        {
            result = "Array is sorted (" + sortType + ").";
        }
        else if ( !sortType.equals("increasing") && !sortType.equals("decreasing") )
        {
            result = "Choose a legal type of sorting.";
        }

        for (int i=1; i < source.length; i++)
        {
            if ( sortType.equals("increasing") && (source[i-1] > source[i]) )
            {
                result = "Array is unsorted (" + sortType + ").";
            }
            else if ( sortType.equals("decreasing") && (source[i-1] < source[i]) )
            {
                result = "Array is unsorted (" + sortType + ").";
            }
        }

        return result;
    }
}
