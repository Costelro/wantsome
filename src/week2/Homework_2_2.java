package week2;

/*
 * ASSIGNMENT 2:
 * Write a function that receives a sorted array and a number
 * and performs binary search on the array to check
 * whether the element is contained therein or not.
 * Note: write two versions of the function:
 * -- one iterative;
 * -- one recursive.
 */


public class Homework_2_2
{
    public static void main(String[] args)
    {
        int[] sourceSortedArray = {-13000, -11, -2, 5, 6, 16, 18};
        int searchedNumber = -2;

        // select one of the 2 binarySearch methods below (both give identical results)
//        int searchResult = binarySearchIterative( sourceSortedArray, searchedNumber );
        int searchResult = binarySearchRecursive( sourceSortedArray, searchedNumber, 0, sourceSortedArray.length );

        if( searchResult >= 0 )
        {
            System.out.printf( "Number %d found at index %d.\n", searchedNumber, searchResult );
        }
        else
        {
            System.out.printf( "Number %d was not found.\n", searchedNumber );
        }
    }


    /**
     * The method does binary search iteratively.
     * @param sortedArray the *sorted* array of int over which the search should be performed
     * @param searchedNumber the int to be found
     * @return the index of searchedNumber in the sortedArray, or -1 if not found
     */
    public static int binarySearchIterative( int[] sortedArray, int searchedNumber )
    {
        int result = -1;

        if( sortedArray == null
            || sortedArray.length == 0
            || searchedNumber < sortedArray[0]
            || searchedNumber > sortedArray[sortedArray.length-1] )
        {
            return result;
        }

        int indexLeftLimit = 0;
        int indexRightLimit = sortedArray.length-1; // inclusive limit (this index can be accessed)

        while( indexLeftLimit <= indexRightLimit )
        {
            int middleIndex = (indexLeftLimit + indexRightLimit) / 2;
            int middleValue = sortedArray[middleIndex];

            if( middleValue == searchedNumber )
            {
                result = middleIndex;
                break;
            }
            else if( searchedNumber < middleValue )
            {
                indexRightLimit = middleIndex - 1;
            }
            else
            {
                indexLeftLimit = middleIndex + 1;
            }
        }

        return result;
    }


    /**
     * The method does binary search recursively.
     * @param sortedArray the *sorted* array of int over which the search should be performed
     * @param searchedNumber the int to be found
     * @param indexLeftLimit the left index of the initial sortedArray (inclusive) from where
     *                       search will be performed on the current method call
     * @param indexRightLimit the right index of the initial sortedArray (exclusive) up to which
     *                        search will be performed on the current method call
     * @return the index of searchedNumber in the sortedArray, or -1 if not found
     */
    public static int binarySearchRecursive( int[] sortedArray, int searchedNumber,
                                             int indexLeftLimit, int indexRightLimit )
    {
        int result;

        if( sortedArray == null
            || sortedArray.length == 0
            || searchedNumber < sortedArray[0]
            || searchedNumber > sortedArray[sortedArray.length-1] )
        {
            result = -1;
            return result;
        }
        if( indexLeftLimit == indexRightLimit )
        {
            result = (sortedArray[indexLeftLimit] == searchedNumber)
                      ? indexLeftLimit
                      : -1;
            return result;
        }

        // the algorithms manipulates indexes over the whole initial sortedArray,
        // and not by creating new & shorter arrays, for efficiency with huge initial arrays
        int indexMiddle = (indexRightLimit - indexLeftLimit) / 2 + indexLeftLimit;
        int middleValue = sortedArray[indexMiddle];
        if( middleValue == searchedNumber )
        {
            result = indexMiddle;
            return result;
        }
        else if( searchedNumber < middleValue )
        {
            return binarySearchRecursive( sortedArray, searchedNumber, indexLeftLimit, indexMiddle );
        }
        else
        {
            return binarySearchRecursive( sortedArray, searchedNumber, indexMiddle+1, indexRightLimit );
        }
    }
}
