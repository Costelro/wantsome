package week2;

/**
 * Created by CostelRo on 01-Jun-18.
 */

/*
 * ASSIGNMENT 8:
 * Write a function that receives an int
 * and returns its base 2 representation as a string.
 * E.g. decimalToBinary(10) -> “1010”, decimalToBinary(127) = “1111111”
 */


public class Homework_2_8
{
    public static void main(String[] args)
    {
        int myDecimalNumber = 127;

        System.out.println( decimalToBinary(myDecimalNumber) );
    }

    private static String decimalToBinary(int number)
    {
        if ( number == 0 || number == 1 ) { return String.valueOf(number); }

        StringBuilder temp = new StringBuilder("");
        int quotient = number;
        while (quotient != 1)
        {
            temp.append( String.valueOf(quotient % 2) );
            quotient = quotient / 2;
        }
        temp.append(quotient);

        return ( temp.reverse().toString() );
    }
}
