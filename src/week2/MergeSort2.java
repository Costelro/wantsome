package week2;


import java.util.Arrays;

public class MergeSort2
{
    private static int[] sortedArray;

    public static void main(String[] args)
    {
//        int[] a1 = {1, 3, 5};
//        int[] a2 = {2, 4, 6, 7};
//        int[] result = mergeArrays(a1, a2);
//        System.out.println(Arrays.toString(result));

        int[] unsortedArray = {38, 27, 43, 3, 9};
        System.out.println("Source:\n" + Arrays.toString(unsortedArray) + "\n");

        sortedArray = new int[unsortedArray.length];

        doMergeSort(unsortedArray, 0, unsortedArray.length-1);

        System.out.println("Sorted Array:\n" + Arrays.toString(sortedArray) + "\n" );
    }


    private static void doMergeSort(int[] source, int left, int right)
    {

        int[] result = new int[source.length];

        while (left < right)
        {
            int middleIndex = (right - left) / 2;

//            int[] arr1 = new int[middleIndex - left];
//            int[] arr2 = new int[source.length - middleIndex - 1];

            int[] arr1 = populateArray(source, left, middleIndex);
            int[] arr2 = populateArray(source, middleIndex+1, right);

            doMergeSort(arr1, 0, arr1.length-1);

            doMergeSort(arr2, 0, arr2.length-1);

            result = mergeArrays(arr1, arr2);
        }

//        return result;
    }


    private static int[] populateArray(int[] source, int left, int right)
    {
        int[] result = new int[right - left + 1];

        int i = 0;
        for (int j = left; j <= right; j++)
        {
            result[i] = source[j];
            i++;
        }

        return result;
    }



    private static int[] mergeArrays(int[] arr1, int[] arr2)
    {

        int[] resultArray = new int[arr1.length + arr2.length];

        int indexArray1 = 0;
        int indexArray2 = 0;
        int indexResult = 0;

        while ( (indexArray1 < arr1.length) && (indexArray2 < arr2.length) )
        {
            if (arr1[indexArray1] <= arr2[indexArray2])
            {
                resultArray[indexResult] = arr1[indexArray1];
                indexArray1++;
                indexResult++;
            }
            else if ( arr2[indexArray2] < arr1[indexArray1] )
            {
                resultArray[indexResult] = arr2[indexArray2];
                indexArray2++;
                indexResult++;
            }
        }

        while (indexArray1 < arr1.length)
        {
            resultArray[indexResult] = arr1[indexArray1];
            indexArray1++;
            indexResult++;
        }

        while (indexArray2 < arr2.length)
        {
            resultArray[indexResult] = arr2[indexArray2];
            indexArray2++;
            indexResult++;
        }

        return resultArray;
    }
}
