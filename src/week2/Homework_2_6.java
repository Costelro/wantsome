package week2;

/**
 * Created by CostelRo on 01-Jun-18.
 */

/* ASSIGNMENT 6:
 * Write a function that takes an array of numbers
 * and prints the numbers that appear on consecutive positions in the array:
 *   e.g for {1, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9} it should print 4, 2, 9
 */


public class Homework_2_6
{
    public static void main(String[] args)
    {
        int[] sourceArray = {1, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9};

        findAndPrintDoubleNumbers(sourceArray);
    }


    private static void findAndPrintDoubleNumbers(int[] source)
    {
        if ( (source == null) || (source.length == 0) || (source.length == 1) )
        {
            System.out.println("Please use array with >= 2 elements.");
        }
        else if (source.length == 2)
        {
            String result = (source[0] == source[1]) ?
                            ("\n" + source[0] + "\n") :
                            "No double numbers found.\n";
            System.out.print(result);
        }
        else
        {
            for (int i=1; i < source.length; i++)
            {
                if ( source[i] == source[i-1] )
                {
                    System.out.print(source[i] + ", ");
                }
            }
        }
    }
}
