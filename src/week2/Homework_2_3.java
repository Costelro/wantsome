package week2;

import java.util.Arrays;


/**
 * Created by CostelRo on 01-Jun-18.
 */

/* ASSIGNMENT 3:
 * Write a function that receives 2 sorted arrays
 * and returns a new sorted array with elements from both arrays.
 * (Tip: this is called the merge algorithm.)
 */


public class Homework_2_3
{
    public static void main(String[] args)
    {
        int[] sortedArray1 = {1, 2, 3, 4, 7, 9};
        int[] sortedArray2 = {0, 2, 5, 6, 8};

        int[] mergedArray = mergeTwoSortedArrays(sortedArray1, sortedArray2);

        System.out.printf( "\nMerged array:\n%s\n\n", Arrays.toString(mergedArray) );
    }

    private static int[] mergeTwoSortedArrays(int[] array1, int[] array2)
    {
        // Treatment of the trivial cases
        if (array1 == null && array2 == null) { return new int[0]; }
        else if (array1 == null && array2 != null) { return array2; }
        else if (array2 == null && array1 != null) { return array1; }
        else if (array1.length == 0 && array2.length > 0) { return array2; }
        else if (array2.length == 0 && array1.length > 0) { return array1; }


        // The general case, with both arrays of length > 0
        int[] result = new int[array1.length + array2.length];

        int currentIndex1 = 0;
        int currentIndex2 = 0;
        int resultIndex = 0;

        while ( (currentIndex1 < array1.length) && (currentIndex2 < array2.length) )
        {
            if (array1[currentIndex1] <= array2[currentIndex2])
            {
                result[resultIndex] = array1[currentIndex1];
                currentIndex1++;
                resultIndex++;
            }
            else
            {
                result[resultIndex] = array2[currentIndex2];
                currentIndex2++;
                resultIndex++;
            }
        }

        // The longest array still has some elements to be processed.
        while (currentIndex1 < array1.length)
        {
            result[resultIndex] = array1[currentIndex1];
            currentIndex1++;
            resultIndex++;
        }

        while (currentIndex2 < array2.length)
        {
            result[resultIndex] = array2[currentIndex2];
            currentIndex2++;
            resultIndex++;
        }

        return result;
    }
}
