package week2;

import java.util.ArrayList;
import java.util.Objects;


/**
 * Created by CostelRo on 03-Jun-18.
 */

/*
 * ASSIGNMENT 9:
 * Starting with the solution for Homework 2_9,
 * write a more general function that receives a number and a base
 * and returns a string with the number’s representation in that base.
 * E.g: decimalToBase(10, 2) -> “1010” | decimalToBase(10, 10) -> “10” | decimalToBase(10, 8) -> “12”
 *
 * Note: For bases higher than 10, you can use letters instead of digits,
 * e.g. for base 16, A = 10, B = 11, …, F = 15.
 *
 * For example: decimalToBase(47, 16) -> “2F”
 */


public class Homework_2_9
{
    /*
     * Class that converts a positive integer in any base 2-37 into any base 2-37.
     * Numbers in base >10 will use some of these characters: ABCDEFGHIJKLMNOPQRSTUVWXYZ.
     */

    public static void main(String[] args)
    {
        String  sourceNumber    = "ffFf";     // must be: positive
        int     sourceBase      = 16;       // must be: between 2 and 37
        int     destinationBase = 28;       // must be: between 2 and 37

        String statusInputValidityCheck = checkInputValidity(sourceNumber, sourceBase, destinationBase);
        if ( "Input numbers valid.".equals( statusInputValidityCheck ) )
        {
            sourceNumber = sourceNumber.toUpperCase(); // normalises sloppy formatting e.g. 33f8FF
            System.out.printf( "%s (base %d) = %s (base %d)\n",
                    sourceNumber,
                    sourceBase,
                    convertIntegerFromBaseToBase( sourceNumber, sourceBase, destinationBase ),
                    destinationBase );
        }
        else
        {
            System.out.println( statusInputValidityCheck );
        }
    }


    private static String checkInputValidity(String sourceNumber, int sourceBase, int destinationBase)
    {
        // Valid numbers: *positive* integers
        // Valid bases: between 2 (binary numbers) and 37 (0..9 and A..Z)
        if ( Objects.equals(sourceNumber, null) || (sourceNumber.length() == 0) )
        {
            return "Error: Use true numbers.";
        }
        else if (sourceBase <= 1 || sourceBase > 36 ||
                destinationBase <= 1 || destinationBase > 36)
        {
            return "Error: Use number bases between 2 and 37.";
        }
        else if ("0".equals(sourceNumber) || "1".equals(sourceNumber))
        {
            return ( String.valueOf(sourceNumber) );
        }
        else if ( sourceBase == destinationBase )
        {
            return ( sourceNumber );
        }
        else
        {
            return "Input numbers valid.";
        }
    }


    private static String convertIntegerFromBaseToBase(
                                        String source,
                                        int sourceBase,
                                        int destinationBase )
    {
        if ( isNumberValidForItsBase(source, sourceBase) )
        {
            int tempConvertToDecimal = convertToDecimal(source, sourceBase);
            if ( destinationBase == 10 )
            {
                return String.valueOf( tempConvertToDecimal );
            }
            else
            {
                return ( convertFromDecimal(tempConvertToDecimal, destinationBase) );
            }
        }
        else
        {
            return ("Error: Incorrect number in the indicated base.");
        }
    }


    private static boolean isNumberValidForItsBase(String source, int sourceBase)
    {
        ArrayList<Character> allowedChars = getValidCharacters(sourceBase);

        for (int i = 0; i < source.length(); i++) {
            if (!allowedChars.contains(source.charAt(i))) {
                return false;
            }
        }

        return true;
    }


    private static ArrayList<Character> getValidCharacters(int sourceBase)
    {
        String usableChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        ArrayList<Character> validChars = new ArrayList<>(16);
        for (int i = 0; i < sourceBase; i++) {
            validChars.add(usableChars.charAt(i));
        }

        return validChars;
    }


    private static int convertToDecimal(String source, int sourceBase)
    {
        int result = 0;
        ArrayList<Character> usableChars = getValidCharacters(sourceBase);

        for (int i = 0; i < source.length(); i++)
        {
            char currentChar = source.charAt(i);

            // Arithmetic value of each char is its index in the list of valid chars.
            int currentCharAsInteger = usableChars.indexOf(currentChar);

            int power = source.length() - 1 - i;
            result += (int) ( currentCharAsInteger * Math.pow(sourceBase, power) );
        }

        return result;
    }


    private static String convertFromDecimal(int source, int destinationBase)
    {
        ArrayList<Character> usableChars = getValidCharacters(destinationBase);

        StringBuilder temp = new StringBuilder("");
        int quotient = source;
        while ( quotient > 1 )
        {
            int remainder = quotient % destinationBase;
            if (remainder > 9)
            {
                temp.append( usableChars.get(remainder) );
            }
            else
            {
                temp.append( String.valueOf(remainder) );
            }

            quotient /= destinationBase;
        }

        // adding the final quotient
        if (quotient == 1)
        {
            temp.append(quotient);
        }

        temp.reverse();
        return ( temp.toString() );
    }

}
