package week2;


/**
 * Created by CostelRo on 01-Jun-18.
 */

/*
 * ASSIGNMENT 4:
 * Write a function that returns a string representation of an array of doubles,
 * e.g [1, 2, 3] -> "[1, 2, 3]".
 * Format number with **two decimals**.
 */


public class Homework_2_4
{
    public static void main(String[] args)
    {
        double[] source = {1.23, 2.261, 3.567};

        System.out.println( doStringRepresentationOfArray(source) );
    }

    private static String doStringRepresentationOfArray(double[] arr)
    {
        StringBuilder result = new StringBuilder("[");
        for (int i=0; i < arr.length-1; i++)
        {
            result.append( String.format("%.2f, ", arr[i]) );
        }
        result.append( String.format("%.2f]", arr[arr.length-1]) );

        return ( result.toString() );
    }
}
