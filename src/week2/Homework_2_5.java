package week2;

import java.util.Arrays;


/**
 * Created by CostelRo on 01-Jun-18.
 */

/* ASSIGNMENT 5:
 * Write a function that takes an array of numbers
 * and returns a new array with only the odd numbers.
 */


public class Homework_2_5
{
    public static void main(String[] args)
    {
        int[] sourceArray = {-15, 14, 0, -1, 4, 8, 13, 27, 34};

        if (sourceArray == null)
        {
            System.out.println("Please use a non-null array.");
        }
        else
        {
            int[] arrayOddNumbers = filterOddNumbersFromArray(sourceArray);
            System.out.println( Arrays.toString(arrayOddNumbers) );
        }
    }


    private static int[] filterOddNumbersFromArray(int[] unfilteredArray)
    {
        int[] result = new int[countOddNumbers(unfilteredArray)];

        int indexInResultArray = 0;
        for (int element : unfilteredArray)
        {
            if ( (element % 2) != 0 )
            {
                result[indexInResultArray] = element;
                indexInResultArray++;
            }
        }

        return result;
    }


    private static int countOddNumbers(int[] sourceArray)
    {
        int counter = 0;

        for (int element : sourceArray)
        {
            counter = counter + (Math.abs(element) % 2);
        }

        return counter;
    }
}
