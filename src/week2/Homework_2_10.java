package week2;

import java.util.Objects;

/**
 * Created by CostelRo on 01-Jun-18.
 */

/*
 * ASSIGNMENT 10:
 * Write a function that receives a number’s string representation in base 2
 * and returns the number itself as an int.
 * E.g. binaryToDecimal(“101”) -> 5
 */


public class Homework_2_10
{
    public static void main(String[] args)
    {
        String myBinaryNumberAsString = "101";

        System.out.println( binaryToDecimal(myBinaryNumberAsString) );
    }


    private static int binaryToDecimal(String source)
    {
        if ( Objects.equals(source, null) || (source.length() == 0) )
        {
            return -1;
        }
        else if ("0".equals(source) || "1".equals(source))
        {
            return Integer.valueOf(source);
        }

        char[] sourceCharArray = source.toCharArray();
        int result = 0;
        for (int i = 0; i < sourceCharArray.length; i++)
        {
            int valueAtIndexI = Character.getNumericValue(sourceCharArray[i]);
            int powerOfTwo = (int) Math.pow(2, sourceCharArray.length - 1 - i);

            result += (valueAtIndexI * powerOfTwo);
        }

        return result;
    }
}
