package week2;

/**
 * Created by CostelRo on 29-May-18.
 */

/*
 * ASSIGNMENT 1:
 * Write a function that receives an array and returns a new array sorted by insertion sort.
 */


public class Homework_2_1
{
    public static void main(String[] args)
    {
        int[] source = {140, 45, 3, 88, 16, 0, 94, 30, -2};

        int[] sortedArray = doInsertionSort(source);

        System.out.println("The sorted array is:");
        for (int element : sortedArray)
        {
            System.out.print(element + ", ");
        }
        System.out.println();
    }


    private static int[] doInsertionSort(int[] originalArray)
    {
        int[] sortedArray = new int[originalArray.length];
        for (int i=0; i < originalArray.length; i++)
        {
            sortedArray[i] = originalArray[i];
        }

        for (int i=1; i < sortedArray.length; i++)
        {
            for (int j=i; j >= 1; j--)
            {
                if ( sortedArray[j-1] > sortedArray[j] )
                {
                    int temp = sortedArray[j];
                    sortedArray[j] = sortedArray[j-1];
                    sortedArray[j-1] = temp;
                }
            }
        }

        return sortedArray;
    }
}
