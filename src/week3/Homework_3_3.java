package week3;


/**
 * Created by CostelRo on 02-Jun-18.
 */

/*
 * ASSIGNMENT 3_3:
 * Write & test code for an implementation of an ArrayList.
 * Note: Define and implement classes, fields and methods according to the requirements file.
 */


interface MyList
{
    // This interface could be made public (and placed in separate file).
    // It has been placed inside the same file as the class only for this exercise.
    int get(int index);
    void put(int index, int value);
    int length();
    boolean contains(int value);
    void addAll(MyList other);
}


public class Homework_3_3
{
    // Used for the testing code.

    public static void main(String[] args)
    {
        MyList testList1 = new ArrayList();

        System.out.printf( "\nElement at index 5 = %d \n", testList1.get(5) );

        testList1.put(5, 123);
        System.out.printf( "Element at index 5 = %d \n", testList1.get(5) );

        testList1.put(15, 42);
        System.out.printf( "Element at index 15 = %d \n", testList1.get(15) );

        System.out.printf( "Element at index 5 = %d \n", testList1.get(5) );

        System.out.printf( "Array length: %d \n", testList1.length() );

        int[] source2 = new int[]{1, 2, 3, 4, 5};
        MyList testList2 = new ArrayList(source2);

        testList1.addAll(testList2);
        System.out.printf( "\nList1 + List2: %s", testList1.toString() );
        testList2.addAll(testList1);
        System.out.printf( "\nList2 + extended-List1: %s \n", testList2.toString() );
    }
}


class ArrayList implements MyList
{
    /* This class could be made public (and placed in separate file).
     * It has been placed inside the same file as the rest only for this exercise.
     */


    // fields
    private int[] internalArray;
    private int initialInternalArrayLength = 5;


    // constructors
    public ArrayList()
    {
        this.internalArray = new int[initialInternalArrayLength];
    }

    public ArrayList(int[] startValuesArray)
    {
        this();

        if (startValuesArray.length > initialInternalArrayLength)
        {
            int index = 0;
            while(index < initialInternalArrayLength)
            {
                this.internalArray[index] = startValuesArray[index];
            }

            while(index < startValuesArray.length)
            {
                this.internalArray[index] = startValuesArray[index];
            }
        }
        else
        {
            int index = 0;
            for (int element : startValuesArray)
            {
                this.internalArray[index] = element;
                index++;
            }
        }
    }


    // getters and setters: none needed


    // other methods
    public int get(int index)
    {
        if ( (index >= 0) && (index < this.length()) ) { return this.internalArray[index]; }
        else return 0;
    }


    public void put(int index, int value)
    {
        // Method to add a value anywhere, including outside the array's current limits!

        if ( (this.internalArray == null) || (this.internalArray.length == 0) )
        {
            this.internalArray = new int[value + 1];
            this.internalArray[index] = value;
        }
        else if (index >= this.internalArray.length)
        {
            this.extendInternalArray(index - this.internalArray.length + 1);
            this.internalArray[index] = value;
        }
        else
        {
            this.internalArray[index] = value;
        }
    }


    public int length()
    {
        return this.internalArray.length;
    }


    public boolean contains(int value)
    {
        // Method returns 'true' at the first finding of 'value'.
        for (int element : this.internalArray)
        {
            if (element == value)
            {
                return true;
            }
        }
        return false;
    }


    public void addAll(MyList other)
    {
        // All elements from 'other' will be put in the new, extended, 'this' array
        // starting right after the elements it had previously (when it was shorter).
        int newIndex = this.internalArray.length;

        this.extendInternalArray( other.length() );

        for (int j = 0; j < other.length(); j++)
        {
            this.internalArray[newIndex] = other.get(j);
            newIndex++;
        }
    }


    public void addAll(int[] sourceArray)
    {
        int oldMaxIndex = this.internalArray.length - 1;

        this.extendInternalArray( sourceArray.length );

        int i = oldMaxIndex;
        for (int j = 0; i < sourceArray.length; j++)
        {
            this.internalArray[i] = sourceArray[j];
            i++;
        }
    }


    private void extendInternalArray(int extensionSize)
    {
        int[] resultArray = new int[this.internalArray.length + extensionSize];
        for (int i=0; i < this.internalArray.length; i++)
        {
            resultArray[i] = this.internalArray[i];
        }

        this.internalArray = resultArray;
    }


    @Override
    public String toString()
    {
        String result = "[";

        StringBuilder strBld = new StringBuilder("");
        for (int i=0; i < this.internalArray.length - 1; i++)
        {
            strBld.append(this.internalArray[i]).append(", ");
        }
        strBld.append( this.internalArray[this.internalArray.length-1] );
        result += strBld.toString();

        result += "]";

        return result;
    }
}
