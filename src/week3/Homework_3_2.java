package week3;

/**
 * Created by CostelRo on 12-Jun-18.
 */

/* ASSIGNMENT 3_2:
 *
 * Implement a class hierarchy for defining shapes, and test it.
 * Note: Define and implement classes, fields and methods according to the detailed file.
 */

public class Homework_3_2
{
    public static void main(String[] args)
    {
        // testing code
        Point testOrigin = new Point( 0, 0 );
        Circle testCircle = new Circle( testOrigin, 3 );
        Triangle testTriangle = new Triangle( testOrigin, new Point(3, 0), new Point(0, 4) );
        Rectangle testRectangle = new Rectangle(testOrigin, new Point(5, 3));
        Shape[] testShapes = {testCircle, testTriangle, testRectangle};

        System.out.printf( "\nTriangle area > Circle area? =  %b\n", testTriangle.isLargerThan(testCircle) );

        double totalArea = 0;
        double totalPerimeter = 0;
        for (Shape s : testShapes)
        {
            totalArea += s.computeArea();
            totalPerimeter += s.computePerimeter();
        }
        System.out.printf( "Sum of all areas = %.2f\n", totalArea );
        System.out.printf( "Sum of all perimeters = %.2f\n\n", totalPerimeter );
    }
}


abstract class Shape
{
    public abstract double computeArea();

    public abstract double computePerimeter();

    public abstract void enlarge(double factor);

    public boolean isLargerThan(Shape shape)
    {
        return (shape == null) || (shape == this) || (this.computeArea() == shape.computeArea());
    }
}


class Point
{
    // fields
    private double xCoordinate;
    private double yCoordinate;


    // constructors
    public Point(double x, double y)
    {
        this.xCoordinate = x;
        this.yCoordinate = y;
    }


    // getters and setters
    public double getXCoordinate()
    {
        return this.xCoordinate;
    }

    public double getYCoordinate()
    {
        return this.yCoordinate;
    }


    // other methods
    public void moveTo(double deltaX, double deltaY)
    {
        // TODO code to check validity of the move, depending on the program context

        this.xCoordinate += deltaX;
        this.yCoordinate += deltaY;
    }

    public double computeDistanceToAnotherPoint(Point other)
    {
        double xDistance = other.xCoordinate - this.xCoordinate;
        double yDistance = other.yCoordinate - this.yCoordinate;
        return Math.sqrt( Math.pow(xDistance, 2) + Math.pow(yDistance, 2) );
    }
}


class Circle extends Shape
{
    // fields
    private Point origin;   // the reference point for each circle
    private double radius;


    // constructors
    public Circle(Point origin, double radius)
    {
        this.origin = origin;
        this.radius = radius;
    }


    // getters and setters
    public Point getOrigin()
    {
        return this.origin;
    }

    public void setOrigin(Point newOrigin)
    {
        if (newOrigin != null) { this.origin = newOrigin; }
    }

    public double getRadius()
    {
        return this.radius;
    }

    public void setRadius(double newRadius)
    {
        if (newRadius > 0) { this.radius = newRadius; }
    }


    // other methods
    public double computeArea()
    {
        return ( Math.PI * this.radius * this.radius );
    }

    public double computePerimeter()
    {
        return ( 2 * Math.PI * this.radius );
    }

    public void enlarge(double factor)
    {
        // Circle enlargement will consider 'origin' as fixed,
        // and the radius will be changed according to the value of the argument 'factor'.
        //
        // Allowed values for 'factor':
        // - must be >-1 (so the circle doesn't disappear or the circle isn't turned "inside out");
        // - must be !=0 (is equivalent to no change).

        if ( (factor > -1) && (factor != 0) ) { this.radius *= factor; }
    }
}


class Triangle extends Shape
{
    // fields
    private Point corner1;  // the reference point for each triangle
    private Point corner2;
    private Point corner3;


    // constructors
    public Triangle(Point corner1, Point corner2, Point corner3)
    {
        this.corner1 = corner1;
        this.corner2 = corner2;
        this.corner3 = corner3;
    }


    // getters and setters
    public Point getCorner1()
    {
        return this.corner1;
    }

    public Point getCorner2()
    {
        return this.corner2;
    }

    public Point getCorner3()
    {
        return this.corner3;
    }


    // other methods
    public double computeArea()
    {
        // Triangle Area calculus will use Heron's formula - details: https://en.wikipedia.org/wiki/Heron%27s_formula
        double semiPerimeter = this.computePerimeter() / 2;

        double side1 = corner1.computeDistanceToAnotherPoint(corner2);
        double side2 = corner2.computeDistanceToAnotherPoint(corner3);
        double side3 = corner3.computeDistanceToAnotherPoint(corner1);

        return Math.sqrt( semiPerimeter *
                          (semiPerimeter - side1) *
                          (semiPerimeter - side2) *
                          (semiPerimeter - side3) );
    }

    public double computePerimeter()
    {
        double side1 = corner1.computeDistanceToAnotherPoint(corner2);
        double side2 = corner2.computeDistanceToAnotherPoint(corner3);
        double side3 = corner3.computeDistanceToAnotherPoint(corner1);

        return ( side1 + side2 + side3 );
    }

    public void enlarge(double factor)
    {
        // Triangle enlargement will consider 'corner1' as fixed,
        // and the other 2 corners will be moved according to the value of the argument 'factor'.
        // The result will be that all 3 sides of the triangle will have their lengths changed by the value of 'factor'.
        //
        // The distance on any axis between the final & the initial position of a moved point is equal to:
        // - 'factor' multiplied by
        // - the distance on that axis between: the initial position of the moving point, and that of the fixed point.
        //
        // Allowed values for 'factor':
        // - must be >-1 (so the sides don't disappear or the triangle isn't turned "inside out");
        // - must be !=0 (is equivalent to no change).

        if ( (factor > -1) && (factor != 0) )
        {
            // move corner2
            double coordinateXOfCorner2 = this.getCorner2().getXCoordinate();
            double coordinateYOfCorner2 = this.getCorner2().getYCoordinate();
            this.corner2.moveTo( coordinateXOfCorner2 * factor, coordinateYOfCorner2 * factor );

            // move corner3
            double coordinateXOfCorner3 = this.getCorner3().getXCoordinate();
            double coordinateYOfCorner3 = this.getCorner3().getYCoordinate();
            this.corner3.moveTo( coordinateXOfCorner3 * factor, coordinateYOfCorner3 * factor );
        }
    }
}


class Rectangle extends Shape
{
    // fields
    private Point corner1;  // the reference point for each rectangle
    private Point corner2;
    private Point corner3;
    private Point corner4;


    // constructors
    public Rectangle(Point corner1, Point corner3)
    {
        // creates a rectangle from its diagonal corners
        this.corner1 = corner1;
        this.corner3 = corner3;
        this.corner2 = new Point(this.corner3.getXCoordinate(), this.corner1.getYCoordinate());
        this.corner4 = new Point(this.corner1.getXCoordinate(), this.corner3.getYCoordinate());
    }


    // getters and setters
    public Point getCorner1()
    {
        return this.corner1;
    }

    public Point getCorner2()
    {
        return this.corner2;
    }

    public Point getCorner3()
    {
        return this.corner3;
    }

    public Point getCorner4()
    {
        return this.corner4;
    }

    public void setCorner1(double newXCoordinate, double newYCoordinate)
    {
        double moveX = newXCoordinate - this.corner1.getXCoordinate();
        double moveY = newXCoordinate - this.corner1.getYCoordinate();
        this.corner1.moveTo(moveX, moveY);
    }

    public void setCorner3(double newXCoordinate, double newYCoordinate)
    {
        double moveX = newXCoordinate - this.corner3.getXCoordinate();
        double moveY = newXCoordinate - this.corner3.getYCoordinate();
        this.corner3.moveTo(moveX, moveY);
    }


    // other methods
    public double computeArea()
    {
        return ( this.corner1.computeDistanceToAnotherPoint(this.corner2) *
                 this.corner1.computeDistanceToAnotherPoint(this.corner4) );
    }

    public double computePerimeter()
    {
        return ( this.corner1.computeDistanceToAnotherPoint(this.corner2) * 2 +
                 this.corner1.computeDistanceToAnotherPoint(this.corner4) * 2 );
    }

    public void enlarge(double factor)
    {
        // Rectangle enlargement will consider 'corner1' as fixed,
        // and the other 3 corners will be moved according to the value of the argument 'factor'.
        // The result will be that all sides of the rectangle will have their lengths changed by the value of 'factor'.
        //
        // Allowed values for 'factor':
        // - must be >-1 (so the sides don't disappear or the triangle isn't turned "inside out");
        // - must be !=0 (is equivalent to no change).

        if ( (factor > -1) && (factor != 0) )
        {
            // move corner3
            double newCoordinateXOfCorner3 = (this.corner3.getXCoordinate() - this.corner1.getXCoordinate()) * factor;
            double newCoordinateYOfCorner3 = (this.corner3.getYCoordinate() - this.corner1.getYCoordinate()) * factor;
            this.corner3.moveTo(newCoordinateXOfCorner3, newCoordinateYOfCorner3);
        }
    }
}
