package week3.homework_3_1;

/**
 * Created by CostelRo on 13.06.2018.
 */


class DiscountFixed extends Discount
{
    // fields: declared in superclass


    // constructors
    public DiscountFixed(String discountName, double discountLevel)
    {
        this.discountID = "disc-" + Discount.currentDiscountIDNumber;
        Discount.currentDiscountIDNumber++;

        this.discountType = DiscountTypes.FIXED;
        this.discountName = discountName;
        this.discountLevel = discountLevel;
    }


    // getters and setters: defined in superclass


    // other methods
    @Override
    double computeDiscountMonetaryValue(double priceFull)
    {
        return this.discountLevel;
    }


    @Override
    public String toString()
    {
        return ( "Discount " + this.discountName + ": " + this.discountLevel + " " + Store.currency );
    }
}
