package week3.homework_3_1;

/**
 * Created by CostelRo on 13.06.2018.
 */


class Product
{
    // fields
    private static int currentProductIDNumber = 1;

    private String productID;
    private String productName;
    private ProductTypes productCategory;
    private double price;


    // constructors
    public Product(String productName, ProductTypes productCategory, double price)
    {
        this.productID = "prod-" + currentProductIDNumber;
        Product.currentProductIDNumber++;

        this.productName = productName;
        this.productCategory = productCategory;
        this.price = price;
    }


    // getters and setters (where justified)
    public String getProductID()
    {
        return this.productID;
    }

    public String getProductName()
    {
        return this.productName;
    }

    public ProductTypes getProductCategory()
    {
        return this.productCategory;
    }

    public void setProductCategory(ProductTypes productCategory)
    {
        this.productCategory = productCategory;
    }

    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }


    // other methods
    @Override
    public String toString()
    {
        return ( this.getProductName() + " (" + this.getProductID() + ")  = " + this.getPrice() + " " + Store.currency +
                 "\n(category: " + this.getProductCategory() + ")\n" );
    }
}
