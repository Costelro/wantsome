package week3.homework_3_1;

import java.util.*;


/**
 * Created by CostelRo on 13.06.2018.
 */


public class Store
{
    // fields
            static String currency = "RON";
    private static Map<String, Product> storeProducts;
    private static Map<String, Discount> storeDiscounts;
    private static Set<Customer> storeCustomers;
    private static Set<Cart> storeActiveCarts;


    // constructors
    public Store()
    {
        Store.storeProducts = new HashMap<>(10);
        Store.storeDiscounts = new HashMap<>(5);
        Store.storeCustomers = new HashSet<>(10);
        Store.storeActiveCarts = new HashSet<>(10);
    }


    // getters and setters: none needed


    // other methods
    static Product getProduct(String productID)
    {
        return Store.storeProducts.get( productID );
    }


    static Discount getDiscount(String discountID)
    {
        return Store.storeDiscounts.get( discountID );
    }


    static Customer getCustomer(String customerID)
    {
        for (Customer c : Store.storeCustomers)
        {
            if ( c.getCustomerCNP().equals( customerID ) )
            {
                return c;
            }
        }

        return null;
    }


    static Cart getCart(String customerID)
    {
        for (Cart c : Store.storeActiveCarts)
        {
            if ( c.getCustomerOwner().getCustomerCNP().equals( customerID ) )
            {
                return c;
            }
        }

        return null;
    }


    Product addProduct(String productName, ProductTypes productCategory, double price)
    {
        Product newProduct = new Product(productName, productCategory, price);
        Store.storeProducts.put( newProduct.getProductID(), newProduct );

        return newProduct;
    }


    void deleteProduct(String productID)
    {
        Store.storeProducts.remove( productID );
    }


    Discount addDiscount(DiscountTypes discountType, String discountName, double discountLevel)
    {
        Discount newDiscount;

        if ( discountType == DiscountTypes.FIXED )
        {
            newDiscount = new DiscountFixed(discountName, discountLevel);
            Store.storeDiscounts.put( newDiscount.discountID, newDiscount );
        }
        else
        {
            // NOTE: From contents of enum DiscountTypes: the only remaining type is "DiscountTypes.PERCENTAGE"
            // So, new if-else branches must be added if that enum gets more values!

            newDiscount = new DiscountPercentage(discountName, discountLevel);
            Store.storeDiscounts.put( newDiscount.discountID, newDiscount );
        }

        return newDiscount;
    }


    void deleteDiscount(String discountID)
    {
        Store.storeDiscounts.remove( discountID );
    }


    Customer addCustomer(String firstName, String lastName, String customerCNP, Address address)
    {
        Customer newCustomer = new Customer(firstName, lastName, customerCNP, address);
        Store.storeCustomers.add( newCustomer );

        return newCustomer;
    }


    // Customers are never deleted, as they might return later


    Cart addCart(Customer customerOwner, List<Product> productsInCart, List<Discount> discountsInCart)
    {
        // for the situations when more than 1 Customer uses their separate Carts simultaneously
        Cart newCart = new Cart(customerOwner, productsInCart, discountsInCart);
        Store.storeActiveCarts.add( newCart );

        return newCart;
    }


    void deleteCart(String customerID)
    {
        // for the situation when the Customer abandons the Cart (automatic deletion after a set time)
        for (Cart c : Store.storeActiveCarts)
        {
            if ( c.getCustomerOwner().getCustomerCNP().equals( customerID ) )
            {
                Store.storeActiveCarts.remove( c );
            }
        }
    }
}
