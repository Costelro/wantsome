package week3.homework_3_1;

/**
 * Created by CostelRo on 13.06.2018.
 */


class Customer
{
    // fields
    private String firstName;
    private String lastName;
    private String customerCNP;
    private Address customerAddress;


    // constructors
    Customer(String firstName, String lastName, String customerCNP, Address address)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.customerCNP = customerCNP;
        this.customerAddress = address;
    }


    // getters and setters (where justified)
    String getFirstName()
    {
        return this.firstName;
    }

    String getLastName()
    {
        return this.lastName;
    }

    String getCustomerCNP()
    {
        return this.customerCNP;
    }

    Address getCustomerAddress()
    {
        return this.customerAddress;
    }

    void setCustomerAddress(Address newCustomerAddress)
    {
        this.customerAddress = newCustomerAddress;
    }


    // other methods
    @Override
    public String toString()
    {
        return ( this.firstName + " " + this.lastName.toUpperCase() + "\n" +
                 this.customerAddress );
    }
}
