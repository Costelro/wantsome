package week3.homework_3_1;


import java.util.ArrayList;

/**
 * Created by CostelRo on 13.06.2018.
 */

/*
 * ASSIGNMENT 3.1:
 * Write a class hierarchy for an online store.
 * Define and implement the entities, fields and methods from the requirements file.
 */


public class Homework_3_1
{
    // the testing code goes here

    public static void main(String[] args)
    {
        // preparation for tests
        Store testStore = new Store();
        Product testProduct1 = testStore.addProduct("Trousers", ProductTypes.CLOTHES, 150);
        Product testProduct2 = testStore.addProduct("Laptop", ProductTypes.ELECTRONICS, 3000);
        Product testProduct3 = testStore.addProduct("Biscuits", ProductTypes.FOOD, 5);

        Address testAddress = new Address("Romania", "Iasi", "Pascani",
                                         "Centrala", "14");
        Customer testCustomer = testStore.addCustomer("James", "Bond",
                                                  "1111111", testAddress);

        Cart testCart = testStore.addCart(testCustomer,
                                          new ArrayList<>(10),
                                          new ArrayList<>(5));

        testCart.addProductToCart(testProduct1.getProductID());
        testCart.addProductToCart(testProduct2.getProductID());
        testCart.addProductToCart(testProduct3.getProductID());

        Discount testDiscount1 = testStore.addDiscount(DiscountTypes.FIXED,
                                           "'Black Friday'", 100);
        Discount testDiscount2 = testStore.addDiscount(DiscountTypes.PERCENTAGE,
                                           "'Christmas preview'", 5);
        testCart.addDiscountToCart(testDiscount1);
        testCart.addDiscountToCart(testDiscount2);

        // test 1
        testCart.printInvoice();

        // test 2
        testCart.removeProductFromCart(testProduct2);
        Product testProduct4 = testStore.addProduct("Blouse", ProductTypes.CLOTHES, 200);
        testCart.addProductToCart(testProduct4.getProductID());
        testCart.printInvoice();
    }
}
