package week3.homework_3_1;

/**
 * Created by CostelRo on 13.06.2018.
 */


class Address
{
    // fields
    private String country;
    private String county;
    private String locality;
    private String street;
    private String streetNumber;


    // constructors
    public Address(String country, String county, String locality, String street, String streetNumber)
    {
        this.country        = country;
        this.county         = county;
        this.locality       = locality;
        this.street         = street;
        this.streetNumber   = streetNumber;
    }


    // getters and setters
    public String getCountry()
    {
        return this.country;
    }

    public void setCountry(String newCountry)
    {
        this.country = newCountry;
    }

    public String getCounty()
    {
        return this.county;
    }

    public void setCounty(String newCounty)
    {
        this.county = newCounty;
    }

    public String getLocality()
    {
        return this.locality;
    }

    public void setLocality(String newLocality)
    {
        this.locality = newLocality;
    }

    public String getStreet()
    {
        return this.street;
    }

    public void setStreet(String newStreet)
    {
        this.street = newStreet;
    }

    public String getStreetNumber()
    {
        return this.streetNumber;
    }

    public void setStreetNumber(String newStreetNumber)
    {
        this.streetNumber = newStreetNumber;
    }


    // other methods
    @Override
    public String toString()
    {
        return "str. " + this.street + " no " + this.streetNumber + "\n" +
                this.locality + ", " + this.county + " county (" + this.country.toUpperCase() + ")";
    }
}
