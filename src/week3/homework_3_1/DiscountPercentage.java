package week3.homework_3_1;

/**
 * Created by CostelRo on 13.06.2018.
 */


class DiscountPercentage extends Discount
{
    // fields: declared in superclass


    // constructors
    public DiscountPercentage(String discountName, double discountLevel)
    {
        this.discountID = "disc-" + Discount.currentDiscountIDNumber;
        Discount.currentDiscountIDNumber++;

        this.discountType = DiscountTypes.PERCENTAGE;
        this.discountName = discountName;
        this.discountLevel = discountLevel;
    }


    // getters and setters: defined in superclass


    // other methods
    @Override
    double computeDiscountMonetaryValue(double priceFull)
    {
        return ( priceFull * this.discountLevel / 100 );
    }


    @Override
    public String toString()
    {
        return ( "Discount " + this.discountName + ": " + this.discountLevel + "%" );
    }
}
