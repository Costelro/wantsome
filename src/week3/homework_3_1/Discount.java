package week3.homework_3_1;

/**
 * Created by CostelRo on 13.06.2018.
 */


abstract class Discount
{
    // fields
    static double currentDiscountIDNumber = 1;

    String discountID;
    DiscountTypes discountType;
    String discountName;            // e.g. "Black Friday 2018"
    double discountLevel;           // the value of discount expressed in various units


    // getters and setters (where justified)
    public String getDiscountID()
    {
        return this.discountID;
    }

    public DiscountTypes getDiscountType()
    {
        return this.discountType;
    }

    public String getDiscountName()
    {
        return this.discountName;
    }

    public void setDiscountName(String newDiscountName)
    {
        this.discountName = newDiscountName;
    }

    public double getDiscountLevel()
    {
        return this.discountLevel;
    }

    public void setDiscountLevel(double newLevel)
    {
        this.discountLevel = newLevel;
    }


    // other methods
    abstract double computeDiscountMonetaryValue(double priceFull);
}
