package week3.homework_3_1;

import java.util.List;

/**
 * Created by CostelRo on 13.06.2018.
 */


class Cart
{
    // fields
    private Customer customerOwner;
    private List<Product> productsInCart;
    private List<Discount> discountsInCart;


    // constructors
    public Cart(Customer customerOwner, List<Product> productsInCart, List<Discount> discountsInCart)
    {
        this.customerOwner = customerOwner;
        this.productsInCart = productsInCart;
        this.discountsInCart = discountsInCart;
    }


    // getters and setters
    public Customer getCustomerOwner()
    {
        return this.customerOwner;
    }


    public void setCustomerOwner(Customer newCustomerOwner)
    {
        // For the situations where shopping started with a temporary & anonymous cart,
        // and the cart changes ownership to a precise customer
        // (at payment or when the client saves the current cart).

        this.customerOwner = newCustomerOwner;
    }


    public List<Product> getProductsInCart()
    {
        return this.productsInCart;
    }


    public List<Discount> getDiscountsInCart()
    {
        return this.discountsInCart;
    }


    // other methods
    void addProductToCart(String newProductID)
    {
        Product newProductForCart = Store.getProduct( newProductID );
        if (newProductForCart != null)
        {
            this.productsInCart.add( newProductForCart );
        }
    }


    void removeProductFromCart(Product productToDelete)
    {
        this.productsInCart.remove( productToDelete );
    }


    void addDiscountToCart(Discount newDiscount)
    {
        this.discountsInCart.add( newDiscount );
    }


    void removeDiscountFromCart(Discount discountToDelete)
    {
        this.discountsInCart.remove( discountToDelete );
    }


    private double computeFinalCartPriceBeforeDiscounts()
    {
        double result = 0;
        for (Product p : this.productsInCart)
        {
            result += p.getPrice();
        }

        return result;
    }


    private double computeFinalCartPriceAfterAllDiscounts(double priceFull)
    {
        double result = priceFull;
        for (Discount d : this.discountsInCart)
        {
            result -= d.computeDiscountMonetaryValue(result);
        }

        return result;
    }


    void printInvoice()
    {
        double priceBeforeDiscounts = this.computeFinalCartPriceBeforeDiscounts();

        String separator1 = "\n==============================\n";
        String separator2 = "\n------------------------------\n";

        System.out.println( separator1 +
                            this.customerOwner +
                            separator2 +
                            this.toString() +
                            separator2 +
                            "Full price = " + priceBeforeDiscounts + " " + Store.currency +
                            separator1 +
                            "TOTAL after discounts = " +
                            this.computeFinalCartPriceAfterAllDiscounts(priceBeforeDiscounts) + " " + Store.currency +
                            separator1 );
    }


    public String toString()
    {
        StringBuilder sb = new StringBuilder("");

        for (Product p : this.productsInCart) { sb.append(p.toString()).append("\n"); }
        for (Discount d : this.discountsInCart) { sb.append(d.toString()).append("\n"); }

        return sb.toString();
    }
}
