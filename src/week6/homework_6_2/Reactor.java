package week6.homework_6_2;

import java.util.Random;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class Reactor
{
    // fields
    private int criticalPowerLevel;
    private int currentPowerLevel;


    // constructors
    Reactor(int criticalPowerLevel)
    {
        if (criticalPowerLevel > 0)
        {
            this.criticalPowerLevel = criticalPowerLevel;
            this.currentPowerLevel = 0;
        }
    }


    // getters and setters
    int getCurrentPowerLevel()
    {
        return currentPowerLevel;
    }


    // other methods
    void increasePower() throws IllegalArgumentException
    {
        Random rand = new Random();
        int increase = rand.nextInt(100);

        this.currentPowerLevel += increase;

        if (this.isReactorCritical())
        {
            System.out.println("5) Danger - power at: " + this.currentPowerLevel);
            throw new IllegalArgumentException("Reactor power: critical level!");
        }
    }


    void decreasePower()
    {
        Random rand = new Random();
        int decrease = rand.nextInt(100);
        if (this.currentPowerLevel - decrease >= 0) { this.currentPowerLevel -= decrease; }
        else { this.currentPowerLevel = 0; }
    }


    private boolean isReactorCritical()
    {
        return this.currentPowerLevel >= this.criticalPowerLevel;
    }
}
