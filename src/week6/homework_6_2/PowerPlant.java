package week6.homework_6_2;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class PowerPlant
{
    // fields
    private int desiredPowerLevel;


    // constructors
    PowerPlant(int desiredPowerOutput)
    {
        if (desiredPowerOutput >= 0) { this.desiredPowerLevel = desiredPowerOutput; }
    }


    // getters and setters
    int getDesiredPowerLevel()
    {
        return this.desiredPowerLevel;
    }

    public void setDesiredPowerLevel(int newPowerLevel)
    {
        if (newPowerLevel >= 0 ) { this.desiredPowerLevel = newPowerLevel; }
    }


    // methods
    void alert()
    {
        System.out.println("6) Alert!");
    }
}
