package week6.homework_6_2;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class Homework_6_2
{
    public static void main(String[] args)
    {
        Reactor testReactor = new Reactor(150);
        PowerPlant testPlant = new PowerPlant(120);
        PlantController testController = new PlantController(testPlant, testReactor);

        testController.runSystem();
    }
}
