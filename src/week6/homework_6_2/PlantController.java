package week6.homework_6_2;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class PlantController
{
    // fields
    private PowerPlant powerPlant;
    private Reactor reactor;


    // constructors
    public PlantController(PowerPlant powerPlant, Reactor reactor)
    {
        this.powerPlant = powerPlant;
        this.reactor = reactor;
    }


    // getters and setters
    PowerPlant getPowerPlant()
    {
        return this.powerPlant;
    }

    Reactor getReactor()
    {
        return this.reactor;
    }


    //other methods
    public void runSystem()
    {
        // runs continuously and check that things goes normally
        System.out.println( "1) Reactor power: " + this.reactor.getCurrentPowerLevel() );
        if (this.reactor.getCurrentPowerLevel() == 0)
        {
            this.reactor.increasePower();
        }

        while (this.reactor.getCurrentPowerLevel() > 0)
        {
            if (needsAdjustment())
            {
                try
                {
                    adjustReactorPower(10);
                }
                catch (IllegalArgumentException iae)
                {
                    this.powerPlant.alert();
                    this.shutdown();
                }
            }
        }
    }


    private boolean needsAdjustment()
    {
        return Math.abs( this.powerPlant.getDesiredPowerLevel() - this.reactor.getCurrentPowerLevel() ) > 10;
    }


    private void adjustReactorPower(int targetDiff) throws IllegalArgumentException
    {
        int needed = this.powerPlant.getDesiredPowerLevel() - this.reactor.getCurrentPowerLevel();
        while (needed > targetDiff)
        {
            if (needed > targetDiff)
            {
                this.reactor.increasePower();
                needed = this.powerPlant.getDesiredPowerLevel() - this.reactor.getCurrentPowerLevel();
                System.out.println( "2) Reactor power: " + this.reactor.getCurrentPowerLevel() );
            }
            if (needed < -targetDiff)
            {
                this.reactor.decreasePower();
                needed = this.powerPlant.getDesiredPowerLevel() - this.reactor.getCurrentPowerLevel();
                System.out.println( "3) Reactor power: " + this.reactor.getCurrentPowerLevel() );
            }
            if (needed <= targetDiff)
            {
                System.out.println("4) Stationary level forever...!");
            }
        }
    }


    private void shutdown()
    {
        while (this.reactor.getCurrentPowerLevel() > 0)
        {
            this.reactor.decreasePower();
        }
        System.out.println("7) Reactor shut down.");
    }
}
