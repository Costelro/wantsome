package week6.homework_6_3;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class Half implements Function
{
    // constructors
    Half(){}


    // other methods
    public int evaluate(int arg)
    {
        return arg / 2;
    }
}
