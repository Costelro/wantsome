package week6.homework_6_3;

/**
 * Created by CostelRo on 22.06.2018.
 */


public interface Function
{
    int evaluate(int arg);
}
