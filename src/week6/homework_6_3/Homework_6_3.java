package week6.homework_6_3;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class Homework_6_3
{
    public static void main(String[] args)
    {
        // test preparations
        int[] testInput = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Function testPrinter = new Print();
        Compute testCompute = new Compute();
        Function testHalf = new Half();


        // test 1
        for (int element : testInput) { testPrinter.evaluate( element ); }
        System.out.println("***");

        // test 2
        int[] processedArray = testCompute.processValues( testInput, testHalf );
        for (int element : processedArray) { testPrinter.evaluate( element ); }
        System.out.println("***");
    }
}
