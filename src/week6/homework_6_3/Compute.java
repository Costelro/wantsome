package week6.homework_6_3;

import java.util.Objects;

/**
 * Created by CostelRo on 22.06.2018.
 */


public class Compute
{
    // constructors
    Compute(){}


    // other methods
    int[] processValues(int[] source, Function processingFunction)
    {
        if (Objects.equals(source, null) || source.length == 0)
        {
            return ( new int[0] );
        }

        int[] result = new int[source.length];

        for (int i=0; i < source.length; i++)
        {
            result[i] = processingFunction.evaluate(source[i]);
        }

        return result;
    }
}
