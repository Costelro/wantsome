package week6.homework_6_1;


import java.util.ArrayList;
import java.util.Objects;


/**
 * Created by CostelRo on 22.06.2018.
 */


public class SensorAverage implements Sensor
{
    // fields
    private ArrayList<Sensor> sensors;
    private String status;


    // constructor
    SensorAverage()
    {
        this.sensors = new ArrayList<>(10);
        this.status = "off";
    }


    // getters and setters
    public ArrayList<Sensor> getSensors()
    {
        return this.sensors;
    }


    // other methods
    void addSensor(Sensor newSensor)
    {
        if (!Objects.equals(newSensor, null)) { this.sensors.add(newSensor); }
    }


    public boolean isOn()
    {
        if ( Objects.equals(this.sensors, null) || this.sensors.size() == 0)
        {
            return false;
        }
        else
        {
            for (Sensor s : this.sensors)
            {
                if (!s.isOn())
                {
                    this.status = "off";
                    return false;
                }
            }
            this.status = "on";
            return true;
        }
    }


    public void on()
    {
        for (Sensor s : this.sensors)
        {
            s.on();
        }
    }


    public void off()
    {
        for (Sensor s : this.sensors)
        {
            if (s instanceof Thermometer) { s.off(); }
        }
    }


    public int measure() throws IllegalArgumentException
    {
        if (this.isOn())
        {
            double sum = 0;
            for (Sensor s : this.sensors)
            {
                int temp = s.measure();
                System.out.println("This sensor temp: " + temp);
                sum += temp;
            }

            System.out.println("Sum of temps: " + sum);
            System.out.println("No of sensors: " + this.sensors.size());
            return (int) (sum / this.sensors.size());
        }
        else
        {
            throw new IllegalArgumentException("Thermometer can't measure, it is off.");
        }
    }
}
