package week6.homework_6_1;


/**
 * Created by CostelRo on 22.06.2018.
 */


public class SensorConstant implements Sensor
{
    // fields
    private final String status = "on";
    private int currentTemperature;


    // constructor
    SensorConstant(int currentTemp)
    {
        this.currentTemperature = currentTemp;
    }


    // getters and setters
    public String getStatus()
    {
        return this.status;
    }


    // other methods
    public boolean isOn() { return "on".equals( this.status ); }


    public void on() {}


    public void off() {}


    public int measure() { return this.currentTemperature; }
}
