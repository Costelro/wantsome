package week6.homework_6_1;

import java.util.Random;


/**
 * Created by CostelRo on 22.06.2018.
 */


public class Thermometer implements Sensor
{
    // fields
    private String status;


    // constructor
    Thermometer()
    {
        this.status = "off";
    }


    // getters and setters
    public String getStatus()
    {
        return this.status;
    }


    // other methods
    public boolean isOn() { return "on".equals( this.status ); }


    public void on() { this.status = "on"; }


    public void off() { this.status = "off"; }


    public int measure() throws IllegalArgumentException
    {
        if ("on".equals( this.status ))
        {
            Random randomGenerator = new Random();
            return randomGenerator.nextInt(61) - 30;
        }
        else
        {
            throw new IllegalArgumentException("Thermometer can't measure because it is off.");
        }
    }
}
