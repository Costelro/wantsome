package week6.homework_6_1;


/**
 * Created by CostelRo on 22.06.2018.
 */

/*
 * ASSIGNMENT 6_1:
 * Write a class that creates multiple instances of the constant and thermometer sensors,
 * and adds them to an average sensor instance.
 * Note: Implement all entities according to the requirements file.
 */


public class Homework_6_1
{
    public static void main(String[] args)
    {
        // test preparations
        Sensor testSensor1 = new SensorConstant(15);
        Thermometer testSensor2 = new Thermometer();
        Thermometer testSensor3 = new Thermometer();
        Thermometer testSensor4 = new Thermometer();

        SensorAverage testSensor5 = new SensorAverage();
        testSensor5.addSensor(testSensor1);
        testSensor5.addSensor(testSensor2);
        testSensor5.addSensor(testSensor3);
        testSensor5.addSensor(testSensor4);


        // tests
        System.out.println( "Current temperature: " + testSensor1.measure() + "\n" );

        try
        {
            testSensor5.measure();
        }
        catch (IllegalArgumentException iae)
        {
            System.out.println("Caught: " + iae.getMessage() + "\n");
        }

        System.out.println( "Average sensor status: " + testSensor5.isOn() );
        testSensor5.on();
        System.out.println( "Average sensor status: " + testSensor5.isOn() + "\n");
        System.out.printf( "Average sensor temperature: %d\n\n", testSensor5.measure() );
    }
}
