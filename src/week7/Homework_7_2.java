package week7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;


/**
 * Created by CostelRo on 05.07.2018.
 */

/*
 * ASSIGNMENT 7_2:
 * Create a program that outputs the links to all Wikipedia.org articles
 * that are reachable in at most 3 steps from a starting article.
 * The list must contain unique links.
 * An article is reachable from another article if a link to it exists.
 */


public class Homework_7_2
{
    public static void main(String[] args)
    {
        String startPage = "https://en.wikipedia.org/wiki/Java";
        String destinationFileName = "multipages--discovered-links.txt";

        long startingTimeMillis = System.currentTimeMillis();

        Map<String, Integer> linksFound = collectLinksFromManyWikipediaPages(startPage, 2);
        writeLinksToFile(linksFound, destinationFileName);

        long endingTimeMillis = System.currentTimeMillis();
        System.out.println("Total Time = " + (endingTimeMillis - startingTimeMillis) / 1000 + " sec.\n\n");
    }


    public static Map<String, Integer> collectLinksFromManyWikipediaPages(String startingURL, int levelUpperLimit )
    {
        HashMap<String, Integer> result = new HashMap<>();
        Map<String, Integer> temp = new HashMap<>();

        // the links found in the starting web page are the first level of pages
        int level = 1;
        for( String link : collectLinksFromOneWikipediaPage(startingURL) )
        {
            result.put( link, 1 );
        }
        System.out.println("-done: level " + level + " (" + result.size() + " links)");

        // getting the links from web pages linked-to from the pages found on previous levels
        while( level < levelUpperLimit )
        {
            for( String link : result.keySet() )
            {
                if( result.get(link) == level )
                {
                    for( String newLink : collectLinksFromOneWikipediaPage(link) )
                    {
                        temp.put( newLink, level+1 );
                    }
                }
            }

            result.putAll( temp );
            temp.clear();
            System.out.println("-done: level " + (level+1) + " (" + result.size() + " links)");
            level++;
        }

        return result;
    }


    /**
     * The method parses the text from a Wikipedia page and looks for link to other articles.
     * @param link a valid link to a Wikipedia page
     * @return an array of Strings representing links to Wikipedia articles
     */
    private static TreeSet<String> collectLinksFromOneWikipediaPage(String link)
    {
        TreeSet<String> result = new TreeSet<>();

        String sourceText = parseWebPageText( link );

        int indexOfLinkStart = 0;
        int indexOfLinkEnd;
        while ( sourceText != null && indexOfLinkStart < sourceText.length() )
        {
            indexOfLinkStart = sourceText.indexOf("href=\"/wiki/", indexOfLinkStart);
            indexOfLinkEnd = sourceText.indexOf( "\"", indexOfLinkStart+12 );

            if ( (indexOfLinkStart != -1) && (indexOfLinkEnd != -1) )
            {
                String partialLink = sourceText.substring( indexOfLinkStart+12, indexOfLinkEnd );
                String fullWikipediaLink = "https://en.wikipedia.org/wiki/" + partialLink;
                result.add( fullWikipediaLink );
            }
            else
            {
                break;
            }

            indexOfLinkStart = indexOfLinkEnd;
        }

        return result;
    }


    /**
     * The method gets all test content from a web page.
     * @param source a String with the link to a web page
     * @return a String with the textual content of the web page
     */
    private static String parseWebPageText( String source )
    {
        if (isArticlePage( source ))
        {
            URLConnection urlConnection = getURLConnection( source );

            if (urlConnection == null)
            {
                return null;
            }

            try ( BufferedReader br = new BufferedReader(new InputStreamReader( urlConnection.getInputStream() )) )
            {
                StringBuilder sb = new StringBuilder();

                String line = br.readLine();
                while (line != null)
                {
                    sb.append( line ).append( System.lineSeparator() );
                    line = br.readLine();
                }

                return sb.toString();
            }
            catch (IOException ioe)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }


    /**
     * The method checks whether the given string is a link to a proper Wikipedia article,
     * and not to another type of page (image, login page etc.).
     * @param webPageAddress a String with a proposed link
     * @return boolean true if the page link starts as a proper article page
     */
    private static boolean isArticlePage( String webPageAddress )
    {
        return ( webPageAddress.startsWith("/wiki/")
                || webPageAddress.startsWith("https://en.wikipedia.org/wiki/")
                || webPageAddress.startsWith("http://en.wikipedia.org/wiki/") );
    }


    private static URLConnection getURLConnection( String url )
    {
        URL newURL = null;
        try
        {
            newURL = new URL( url );
        }
        catch (MalformedURLException murle)
        {
            return null;
        }

        try
        {
            return newURL.openConnection();
        }
        catch (IOException ioe)
        {
            return null;
        }
    }


    private static String writeLinksToFile( Map<String, Integer> links, String destinationFileName )
    {
        int counter = 1;

        try( BufferedWriter writer = new BufferedWriter( new FileWriter( destinationFileName ) ) )
        {
            String lineSeparator = System.lineSeparator();
            StringBuilder sb = new StringBuilder();
            sb.append( lineSeparator ).append( "=========================" ).append( lineSeparator );
            sb.append( "  LINKS FOUND: " ).append( links.size() ).append( lineSeparator );
            sb.append( "=========================" ).append( lineSeparator ).append( lineSeparator );

            for( String link : links.keySet() )
            {
                sb.append( counter ).append( ". " )
                                    .append( "(level " ).append( links.get(link) ).append(") ")
                                    .append( link ). append( lineSeparator );
                counter++;
            }

            writer.write( sb.toString() );
        }
        catch( IOException ioe )
        {
            return ioe.getMessage();
        }

        return String.valueOf( counter );
    }
}
