package week7;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by CostelRo on 04.07.2018.
 */


/*
 * ASSIGNMENT 7_1:
 * Create a program that reads a file containing a list of links to Wikipedia articles
 * and outputs the links to other articles contained in those pages.
 *
 * Suggested steps:
 * - Read input file: result: set, or list, or array of strings
 * - Download an article given an url
 * - Search for articles in a string that represents the HTML source code of an article
 * - Scan the code for links
 * - Output to a file
 */


public class Homework_7_1
{
    public static void main(String[] args)
    {
        String sourceFileName = "source-links.txt";
        List<String> sourceLinks = getSourceLinks( sourceFileName );

        String destinationFileName = "discovered-links.txt";
        Set<String> foundLinks = discoverLinksInPage( sourceLinks );

        writeLinksToFile( foundLinks, destinationFileName );
    }


    /**
     * Collects all links from a text file containing properly formatted links.
     * Returns all unique links (no duplicates).
     */
    private static List<String> getSourceLinks( String sourceFile )
    {
        List<String> result = new ArrayList<>();

        TreeSet<String> temp = new TreeSet<>();
        try( BufferedReader br = new BufferedReader( new FileReader( sourceFile )) )
        {
            String line;
            while ( (line = br.readLine()) != null )
            {
                // some programs, including Microsoft Notepad,
                // add a byte order mark '\uFEFF' at the beginning of UTF-encoded files
                // (which should have been stripped automatically, but Java doesn't do that).
                if (line.startsWith("\uFEFF"))
                {
                    line = String.copyValueOf(line.toCharArray(), 1, line.length()-1);
                }
                temp.add( line );
            }
        }
        catch ( IOException ioe)
        {
            System.out.println( "Source file inaccessible: " + ioe.getMessage() );
            return result;
        }

        temp.remove("");            // eliminates any empty lines in the source file
        result.addAll( temp );

        return result;
    }


    /**
     * The method parses the text from some Wikipedia pages and looks for links to other articles.
     * @param links a list of links to Wikipedia pages
     * @return an array of Strings representing links to Wikipedia articles
     */
    private static TreeSet<String> discoverLinksInPage(List<String> links )
    {
        TreeSet<String> result = new TreeSet<>();

        for (String aLink : links)
        {
            String sourceText = parseWebPageText( aLink );

            int indexStartLink = 0;
            while ( sourceText != null && indexStartLink < sourceText.length() )
            {
                indexStartLink = sourceText.indexOf("href=\"/wiki/", indexStartLink);
                int indexEndLink = sourceText.indexOf( "\"", indexStartLink+12 );
                if (indexStartLink != -1)
                {
                    if (indexEndLink != -1)
                    {
                        String partialLink = sourceText.substring( indexStartLink+12, indexEndLink );
                        String fullWikipediaLink = "https://en.wikipedia.org/wiki/" + partialLink;
                        result.add( fullWikipediaLink );
                    }
                }
                else
                {
                    break;
                }

                indexStartLink = indexEndLink;
            }
        }

        return result;
    }


    /**
     * The method gets all test content from a web page.
     * @param source a String with the link to a web page
     * @return a String with the textual content of the web page
     */
    private static String parseWebPageText(String source )
    {
        if (isArticlePage( source ))
        {
            URLConnection urlConnection = getURLConnection( source );

            if (urlConnection == null)
            {
                return null;
            }

            try ( BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream())) )
            {
                StringBuilder sb = new StringBuilder();

                String line = br.readLine();
                while (line != null)
                {
                    sb.append( line ).append( System.lineSeparator() );
                    line = br.readLine();
                }

                return sb.toString();
            }
            catch (IOException ioe)
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }


    /**
     * The method checks whether the given string is a link to a proper Wikipedia article,
     * and not to another type of page (image, login page etc.).
     * @param webPageAddress a String with a proposed link
     * @return boolean true if the page link starts as a proper article page
     */
    private static boolean isArticlePage( String webPageAddress )
    {
        return ( webPageAddress.startsWith("/wiki/")
                 || webPageAddress.startsWith("https://en.wikipedia.org/wiki/")
                 || webPageAddress.startsWith("http://en.wikipedia.org/wiki/") );
    }


    private static URLConnection getURLConnection(String url)
    {
        URL newURL = null;
        try
        {
            newURL = new URL( url );
        }
        catch (MalformedURLException murle)
        {
            return null;
        }

        try
        {
            return newURL.openConnection();
        }
        catch (IOException ioe)
        {
            return null;
        }
    }


    private static String writeLinksToFile(Set<String> links, String destinationFileName)
    {
        int counter = 1;

        try ( BufferedWriter writer = new BufferedWriter( new FileWriter( destinationFileName ) ) )
        {
            StringBuilder sb = new StringBuilder();
            String separator = System.lineSeparator();
            sb.append( separator ).append( "=========================" ).append( separator );
            sb.append( "  LINKS FOUND: " ).append( links.size() ).append( separator );
            sb.append( "=========================" ).append( separator ).append( separator );

            for (String link : links)
            {
                sb.append( counter ). append( ". " ).append( link ). append( separator );
                counter += 1;
            }

            writer.write( sb.toString() );
        }
        catch (IOException ioe)
        {
            return ioe.getMessage();
        }

        return String.valueOf( counter );
    }
}
