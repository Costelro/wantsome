package week4.homework_4_2;

/**
 * Created by CostelRo on 15.06.2018.
 */


class Card
{
    // fields
    private final CardNumber cardNumber;
    private final CardSuit cardSuit;


    // constructors
    Card(CardNumber cardNumber, CardSuit cardSuit)
    {
        this.cardNumber = cardNumber;
        this.cardSuit = cardSuit;
    }


    // getters and setters
    public CardNumber getCardNumber() { return this.cardNumber; }

    public CardSuit getCardSuit() { return this.cardSuit; }


    // other methods
    @Override
    public String toString()
    {
        return ( this.cardNumber.getCardSymbol() + this.cardSuit.getSuitSymbol() );
    }
}
