package week4.homework_4_2;

/**
 * Created by CostelRo on 15.06.2018.
 */


public enum CardNumber
{
    TWO     (2,   "2"),
    THREE   (3,   "3"),
    FOUR    (4,   "4"),
    FIVE    (5,   "5"),
    SIX     (6,   "6"),
    SEVEN   (7,   "7"),
    EIGHT   (8,   "8"),
    NINE    (9,   "9"),
    TEN     (10, "10"),
    ACE     (11,  "A"),
    JACK    (12,  "J"),
    QUEEN   (13,  "Q"),
    KING    (14,  "K")
    ;


    // fields
    private final int cardValue;
    private final String cardSymbol;


    // constructors
    CardNumber(int cardValue, String cardSymbol)
    {
        this.cardValue = cardValue;
        this.cardSymbol = cardSymbol;
    }


    // getters and setters (where needed)
    public int getCardValue() { return this.cardValue; }

    public String getCardSymbol() { return this.cardSymbol; }


    // other methods



//    @Override
//    public String toString()
//    {
//        return this.toString();
//    }


}
