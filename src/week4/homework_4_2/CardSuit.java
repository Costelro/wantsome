package week4.homework_4_2;

/**
 * Created by CostelRo on 15.06.2018.
 */


public enum CardSuit
{
    DIAMONDS    ('\u2662'),
    CLUBS       ('\u2663'),
    HEARTS      ('\u2661'),
    SPADES      ('\u2660')
    ;


    // fields
    private final char suitSymbol;


    // constructors
    CardSuit(char cardSuit) { this.suitSymbol = cardSuit; }


    // getter and setters (where needed)
    public char getSuitSymbol() { return this.suitSymbol; }


    // other methods
    @Override
    public String toString()
    {
        return ( "of " + this.name() );
    }
}
