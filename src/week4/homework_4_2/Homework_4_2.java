package week4.homework_4_2;

/**
 * Created by CostelRo on 15.06.2018.
 */

/*
 * ASSIGNMENT 4_2:
 * Model a pack of playing cards with basic operations on them.
 * Note: Define and implement the classes, fields and methods needed from the requirements file.
 */


public class Homework_4_2
{
    public static void main(String[] args)
    {
        // preparations for testing
        CardsDeck testDeck = new CardsDeck();

        // tests
        System.out.println( testDeck.dealHand(5) );
        System.out.println( testDeck.dealHand(5) );
        System.out.println( testDeck.dealHand(5) );
        System.out.println("Used cards: " + testDeck.getUsedCardsCount());
        System.out.println("Available cards: " + testDeck.getAvailableCardsCount() + "\n");

        System.out.println( testDeck.dealHand(50) );
        System.out.println("Used cards: " + testDeck.getUsedCardsCount());
        System.out.println("Available cards: " + testDeck.getAvailableCardsCount() + "\n");

        System.out.println( testDeck.dealHand(50) );
        System.out.println("Used cards: " + testDeck.getUsedCardsCount());
        System.out.println("Available cards: " + testDeck.getAvailableCardsCount() + "\n");

        testDeck.shuffle();
        System.out.println( testDeck.dealHand(5) );
        System.out.println("Used cards: " + testDeck.getUsedCardsCount());
        System.out.println("Available cards: " + testDeck.getAvailableCardsCount() + "\n");
    }
}
