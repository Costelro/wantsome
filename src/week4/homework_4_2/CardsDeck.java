package week4.homework_4_2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by CostelRo on 15.06.2018.
 */


public class CardsDeck
{
    //fields
    private List<Card> availableCards = new ArrayList<>(52);
    private List<Card> usedCards = new ArrayList<>(52);


    // constructors
    public CardsDeck()
    {
        for ( CardNumber newCardNumber : CardNumber.values() )
        {
            for ( CardSuit newCardSuit : CardSuit.values() )
            {
                this.availableCards.add( new Card(newCardNumber, newCardSuit) );
            }
        }

        Collections.shuffle( this.availableCards );
    }


    // getters and setters (where needed)
    public List<Card> getAvailableCards() { return this.availableCards; }

    public List<Card> getUsedCards() { return this.usedCards; }


    // other methods
    List<Card> dealHand(int cards)
    {
        ArrayList<Card> newHand;
        if (this.availableCards.size() == 0)            { return new ArrayList<>(); }
        else if (this.availableCards.size() >= cards)   { newHand = dealCard(cards); }
        else                                            { newHand = dealCard( this.availableCards.size() ); }

        return newHand;
    }


    private ArrayList<Card> dealCard(int number)
    {
        ArrayList<Card> dealtCards = new ArrayList<>(number);

        for (int i=1; i <= number; i++)
        {
            Card newDealtCard = this.availableCards.remove(0);
            this.usedCards.add( newDealtCard );
            dealtCards.add( newDealtCard );
        }

        return dealtCards;
    }


    void shuffle()
    {
        // collects again all 52 cards, making them available again
        this.availableCards.addAll( this.usedCards );
        this.usedCards.clear();

        // prepares cards for dealing by shuffling them in place
        Collections.shuffle( this.availableCards );
    }


    int getAvailableCardsCount() { return this.getAvailableCards().size(); }


    int getUsedCardsCount() { return this.usedCards.size(); }
}
