package week4;

/**
 * Created by CostelRo on 09-Jun-18.
 */

/*
 * ASSIGNMENT 4_1:
 * Create a Complex class that represents a complex number with the following methods:
  * - Complex add(Complex number): adds this and number and returns a new Complex instance;
  *     tip: (a+bi) + (c+di) = (a+c) + (b+d)i
  * - Complex negate(): returns a new Complex number representing the -this;
  *     tip: if z = (a+bi) then negated z is (-a-bi)
  * - Complex multiply(Complex number): returns a new complex number that is equal to this * number;
  *     tip: (a+bi)(c+di) = (ac−bd) + (ad+bc)i
  * - equals(Complex number)
  * - toString(): should return strings such as “3 + 4i”
  * - static create(double realPart, double imaginaryPart)
  * NOTE: The complex numbers must be immutable, so the class must return new complex numbers as results.
 */


public class Homework_4_1
{
    public static void main(String[] args)
    {
        Complex c1 = Complex.create(1.1, 2.2);
        Complex c2 = Complex.create(3.3, 4.4);

        System.out.printf( "\nNegated %s = %s\n", c1, c1.negate() );
        System.out.printf( "%s + %s = %s\n", c1, c2, c1.add(c2) );
        System.out.printf( "%s - %s = %s\n", c1, c2, c1.subtract(c2) );
        System.out.printf( "%s * %s = %s\n", c1, c2, c1.multiply(c2) );
        System.out.printf( "%s is %s equal to %s.\n\n", c1, (c1.equals(c2) ? "\b" : "not"), c2 );
    }
}


class Complex
{
    /*
     * Class models an imaginary number, such as "4 + 5i"
     */


    // fields
    private double real = 0;
    private double imaginary = 0;


    // constructors: implementing the Builder pattern
    private Complex(double realPart, double imaginaryPart)
    {
        this.real = realPart;
        this.imaginary = imaginaryPart;
    }

    static Complex create(double realPart, double imaginaryPart)
    {
        // first does some important checks (nothing, in this case), then calls the constructor
        return ( new Complex(realPart, imaginaryPart) );
    }


    // getters and setters: none, as this class creates immutable objects


    // methods
    public Complex add(Complex other)
    {
        double resultReal = this.real + other.real;
        double resultImaginary = this.imaginary + other.imaginary;

        return create(resultReal, resultImaginary);
    }

    public Complex negate()
    {
        double resultReal = -this.real;
        double resultImaginary = -this.imaginary;

        return create(resultReal, resultImaginary);
    }

    public Complex subtract(Complex other)
    {
        return this.add(other.negate());
    }

    public Complex multiply(Complex other)
    {
        double resultReal = (this.real * other.real) - (this.imaginary * other.imaginary);
        double resultImaginary = (this.real * other.imaginary) + (this.imaginary * other.real);

        return create(resultReal, resultImaginary);
    }

    public boolean equals(Complex other)
    {
        if (this == other) return true;
        if (other == null) return false;

        return ( (this.real == other.real) && (this.imaginary == other.imaginary) );
    }

    @Override
    public String toString()
    {
        if ( this.real == 0 && this.imaginary == 0 ) { return "0"; }

        String realPart = myPrettyFormatNumbers(this.real);
        if (this.real != 0 && this.imaginary == 0)   { return realPart; }

        String operator = "+";

        String imaginaryPart = String.valueOf(this.imaginary);
        if (this.imaginary < 0)
        {
            operator = "-";
            imaginaryPart = String.valueOf(-this.imaginary);
        }

        imaginaryPart = myPrettyFormatNumbers( Double.valueOf(imaginaryPart) );
        if (this.real == 0 && this.imaginary != 0)   { return ( "-".equals(operator) ?
                                                                "-" :
                                                                "" ) +
                                                              imaginaryPart + "i"; }
        else                                         { return ( realPart +
                                                                operator +
                                                                imaginaryPart + "i" );}
    }

    private String myPrettyFormatNumbers(double sourceNumber)
    {
        long temp = (long) sourceNumber;
        if( sourceNumber == temp ) { return String.valueOf(temp); }
        else { return String.format("%.1f", sourceNumber); } // needs more work at rounding, e.g. for 6.2000001
    }

}
