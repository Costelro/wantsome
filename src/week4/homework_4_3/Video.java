package week4.homework_4_3;

import java.util.Objects;

/**
 * Created by CostelRo on 15.06.2018.
 */


class Video extends MediaEntity
{
    // fields
    private int     id;
    private String  duration;
    private boolean fullHD;


    // constructors
    Video(Library library, String title, String duration, boolean fullHD)
    {
        this.id = MediaEntity.newGlobalID++;
        this.library = library;
        this.mediaType = MediaTypes.VIDEO;
        this.title = title;
        this.duration = duration;
        this.fullHD = fullHD;
        this.noOfDownloads = 0;
        this.updateNoOfDownloads(this.noOfDownloads); // useful to populate Top20 at the start
    }


    // getters and setters
    public int getID()
    {
        return this.id;
    }

    public String getDuration()
    {
        return this.duration;
    }

    void setDuration(String newDuration)
    {
        this.duration = newDuration;
    }

    public boolean isFullHD()
    {
        return this.fullHD;
    }

    void setFullHD(boolean newFullHDStatus)
    {
        this.fullHD = newFullHDStatus;
    }


    // other methods
    @Override
    public String toString()
    {
        String fullHDStatus = (this.fullHD) ? "FullHD" : "SD";

        return ( "\"" + this.title + "\"\n"
                 + "(" + this.getMediaType().name().toLowerCase() + ", " + this.duration + ", " + fullHDStatus + ")\n" );
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Video other = (Video) o;

        return Objects.equals( this.duration, other.duration );
    }
}