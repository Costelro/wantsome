package week4.homework_4_3;

/**
 * Created by CostelRo on 15.06.2018.
 */


abstract class MediaEntity implements Comparable
{
    // fields
    static  int         newGlobalID = 0;        // automatically incremented at each use in the subclasses
            Library     library;
            MediaTypes  mediaType;
            String      title;
            Integer     noOfDownloads;


    // constructors
    MediaEntity(){}     // instances are needed only from its subclasses


    // getters and setters (where needed)
    public abstract int getID();


    public MediaTypes getMediaType()
    {
        return this.mediaType;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(String newTitle)
    {
        this.title = newTitle;
    }

    public Integer getNoOfDownloads()
    {
        return this.noOfDownloads;
    }

    void setNoOfDownloads(int initialDownloads)
    {
        this.noOfDownloads = (initialDownloads > 0) ? initialDownloads : 0;
    }


    // other methods
    void updateNoOfDownloads(int additionalDownloads)
    {
        this.noOfDownloads += (additionalDownloads > 0) ? additionalDownloads : 0;
    }


    @Override
    public int compareTo(Object o)
    {
        if (o != null)
        {
            int compareDownloads = this.noOfDownloads.compareTo( ((MediaEntity) o).noOfDownloads );
            int compareTitles = this.title.compareTo( ((MediaEntity) o).title );

            if (compareDownloads == 0) { return compareTitles; }
            else if (compareDownloads < 0) { return -1; }
            else { return 1; }
        }
        else { return 1; }
    }
}