package week4.homework_4_3;

import java.util.*;

/**
 * Created by CostelRo on 15.06.2018.
 */


public class Library
{
    // fields
    private ArrayList<MediaEntity> archive;


    // constructors
    public Library()
    {
        this.archive = new ArrayList<>(30);
    }


    // getters and setters
    public List<MediaEntity> getArchive()
    {
        return this.archive;
    }

    public void setArchive(ArrayList<MediaEntity> archive)
    {
        this.archive = archive;
    }


    // other methods
    MediaEntity getMediaEntity(int index)
    {
        if ( (index >= 0) && (index < this.archive.size()) ) { return this.archive.get(index); }
        return null;
    }


    public Book addBook(Library library, String title, String author, String publishingHouse)
    {
        Book newBook = new Book(library, title, author, publishingHouse);
        this.archive.add( newBook.getID(), newBook );

        return newBook;
    }


    public Video addVideo(Library library, String title, String duration, boolean fullHDStatus)
    {
        Video newVideo = new Video(library, title, duration, fullHDStatus);
        this.archive.add( newVideo.getID(), newVideo );

        return newVideo;
    }


    public Mp3 addMp3(Library library, String title, String singer, String album)
    {
        Mp3 newMp3 = new Mp3(library, title, singer, album);
        this.archive.add( newMp3.getID(), newMp3 );

        return newMp3;
    }


    public void deleteMediaEntity(int entityID)
    {
        this.archive.remove( entityID );
    }



    void displayTop5()
    {
        // must copy the whole list of MediaEntities and then sort the copy (by number of downloads)
        // since the sort changes the list itself
        ArrayList<MediaEntity> temp = new ArrayList<>( this.archive.size() );
        temp.addAll( this.archive );
        Comparator<MediaEntity> comp = new DownloadsReversedComparator();
        temp.sort( comp );

        // displaying the Top5
        displayListOfMediaEntities( temp, "5", "Top 5 by downloads" );
    }


    void displayLibrary(ArrayList<MediaEntity> list)
        {
            /*
             * The elements in the list are implicitly sorted by ID (increasing).
             * The argument 'list' can be 'null', resulting in the entire collection being displayed.
             */
            if (Objects.equals(list, null)) { list = this.archive; }
            displayListOfMediaEntities( list, "All media entities");
        }


    void displayListOfMediaEntities(ArrayList<MediaEntity> list, String numberOfEntities, String title)
    {
        String separator1 = "==============================";
        String separator2 = "------------------------------";

        System.out.println(separator1);
        System.out.println( title.toUpperCase() );

        int localNumber = 0;
        ArrayList<MediaEntity> localList = list;
        if (list != null) { localNumber = list.size(); }
        else { localList = new ArrayList<>(0); }
        if (numberOfEntities == null || "".equals(numberOfEntities)) { localNumber = localList.size(); }
        else { localNumber = Integer.valueOf(numberOfEntities); }

        for (int i=0; i < localNumber; i++)
        {
            System.out.print( i+1 + ") " + localList.get(i).getNoOfDownloads() + " downloads:\n" );
            System.out.printf( localList.get( i ).toString() );
            if (i < localNumber-1) { System.out.println(separator2); }
        }

        System.out.println(separator1);
    }


    void displayListOfMediaEntities(ArrayList<MediaEntity> list, String title)
    {
        String separator1 = "==============================";
        String separator2 = "------------------------------";

        System.out.println(separator1);
        System.out.println( title.toUpperCase() );

        ArrayList<MediaEntity> localList = list;
        int localNumber = 0;
        if (list != null) { localNumber = list.size(); }
        else { localList = new ArrayList<>(0); }

        for (int i=0; i < localNumber; i++)
        {
            System.out.print( i+1 + ") " + localList.get(i).getNoOfDownloads() + " downloads:\n" );
            System.out.printf( localList.get( i ).toString() );
            if (i < localNumber-1) { System.out.println(separator2); }
        }

        System.out.println(separator1);
    }


    ArrayList<MediaEntity> searchLibrary(String searchString)
    {
        if ( searchString == null || searchString.length() == 0 )
        {
            return new ArrayList<>(0);
        }

        ArrayList<MediaEntity> topResults = new ArrayList<>(5);
        ArrayList<MediaEntity> otherResults = new ArrayList<>(5);
        String[] searchWords = searchString.toLowerCase().split("\\s+");

        for (MediaEntity me : this.archive)
        {
            boolean isSpecial = false;  // indicates if the search seems to look for a category of results
            boolean hasMatch = false;   // indicates if a word other than the special (about category) matches

            String entityContentsAsString = getEntityContentAsOneString(me);
            String[] entityContentsAsArray = entityContentsAsString.split("\\s+");
            Set<String> entityContentsAsSet = new TreeSet<>();
            Collections.addAll(entityContentsAsSet, entityContentsAsArray); // eliminates duplicate words
            int lengthEntityContents = entityContentsAsSet.size();

            String[] searchStringAsArray = searchString.split("\\s+");
            Set<String> searchStringAsSet = new TreeSet<>();
            Collections.addAll(searchStringAsSet, searchStringAsArray); // eliminates duplicate words

            if ( (me.getMediaType() == MediaTypes.BOOK
                    && (searchStringAsSet.contains("book") || searchStringAsSet.contains("ebook")))
                 || (me.getMediaType() == MediaTypes.VIDEO
                    && (searchStringAsSet.contains("video") || searchStringAsSet.contains("movie")))
                 || (me.getMediaType() == MediaTypes.MP3
                    && (searchStringAsSet.contains("mp3") || searchStringAsSet.contains("music"))) )
            {
                isSpecial = true;
                searchStringAsSet.remove( me.getMediaType().toString().toLowerCase() );
            }

            int lengthSearchString = searchStringAsSet.size();
            entityContentsAsSet.addAll(searchStringAsSet); // eliminates duplicate words from the 2 sets
            if (entityContentsAsSet.size() < lengthEntityContents + lengthSearchString)
            {
                hasMatch = true;
            }

            if (hasMatch)
            {
                if (isSpecial)
                {
                    topResults.add( me );
                }
                else
                {
                    otherResults.add( me );
                }
            }
            else if (isSpecial)
            {
                otherResults.add( me );
            }
        }

        Comparator<MediaEntity> comp = new DownloadsReversedComparator();
        topResults.sort(comp);
        otherResults.sort(comp);
        topResults.addAll( otherResults );

        return topResults;
        }


    private String getEntityContentAsOneString(MediaEntity entity)
    {
        String result = entity.getTitle();

        if (entity.getMediaType() == MediaTypes.BOOK)
        {
            String equivalentTerms = " book ebook ";
            Book temp = (Book) entity;
            result += equivalentTerms + temp.getAuthor() + " " + temp.getPublishingHouse();
        }
        else if (entity.getMediaType() == MediaTypes.VIDEO)
        {
            String equivalentTerms = " video movie ";
            result += equivalentTerms;
        }
        else if (entity.getMediaType() == MediaTypes.MP3)
        {
            String equivalentTerms = " music mp3 ";
            Mp3 temp = (Mp3) entity;
            result += equivalentTerms + temp.getSinger() + " " + temp.getAlbum();
        }

        return result.toLowerCase();
    }
}
