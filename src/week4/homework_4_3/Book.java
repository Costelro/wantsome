package week4.homework_4_3;

/**
 * Created by CostelRo on 15.06.2018.
 */


class Book extends MediaEntity
{
    // fields
    private int     id;
    private String  author;
    private String  publishingHouse;


    // constructors
    Book(Library library, String title, String author, String publishingHouse)
    {
        this.id = MediaEntity.newGlobalID++;
        this.library = library;
        this.mediaType = MediaTypes.BOOK;
        this.title = title;
        this.author = author;
        this.publishingHouse = publishingHouse;
        this.noOfDownloads = 0;
        this.updateNoOfDownloads( this.noOfDownloads ); // useful to populate Top20 at the start
    }


    // getters and setters
    public int getID()
    {
        return this.id;
    }

    public String getAuthor()
    {
        return this.author;
    }

    void setAuthor(String newAuthor)
    {
        this.author = newAuthor;
    }

    public String getPublishingHouse()
    {
        return this.publishingHouse;
    }

    void setPublishingHouse(String newPublishingHouse)
    {
        this.publishingHouse = newPublishingHouse;
    }


    // other methods
    @Override
    public String toString()
    {
        return ( "\"" + this.title + "\"\n"
                 + this.author
                 + " (" + this.getMediaType().name().toLowerCase() + ", " + this.publishingHouse + ")\n" );
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Book other = (Book) o;
        if (this.compareTo( other ) != 0) { return false; }
        if ( (this.author != null)
             ? !this.author.equals(other.author)
             : other.author != null )
        {
            return false;
        }

        return ( (this.publishingHouse != null)
                 ? this.publishingHouse.equals(other.publishingHouse)
                 : other.publishingHouse == null );
    }
}
