package week4.homework_4_3;

/**
 * Created by CostelRo on 15.06.2018.
 */


public enum MediaTypes
{
    BOOK,
    VIDEO,
    MP3
}
