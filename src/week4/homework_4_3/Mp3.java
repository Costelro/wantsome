package week4.homework_4_3;

/**
 * Created by CostelRo on 15.06.2018.
 */


class Mp3 extends MediaEntity
{
    // fields
    private int     id;
    private String  singer;
    private String  album;


    // constructors
    Mp3(Library library, String title, String singer, String album)
    {
        this.id = MediaEntity.newGlobalID++;
        this.library = library;
        this.mediaType = MediaTypes.MP3;
        this.title = title;
        this.singer = singer;
        this.album = album;
        this.noOfDownloads = 0;
        this.updateNoOfDownloads(this.noOfDownloads); // useful to populate Top20 at the start
    }


    // getters and setters
    public int getID()
    {
        return this.id;
    }

    public String getSinger()
    {
        return this.singer;
    }

    void setSinger(String newSinger)
    {
        this.singer = newSinger;
    }

    public String getAlbum()
    {
        return this.album;
    }

    void setAlbum(String newAlbum)
    {
        this.album = newAlbum;
    }


    // other methods
    @Override
    public String toString()
    {
        return ( "\"" + this.title + "\"\n"
                 + this.singer
                 + " (" + this.getMediaType().name().toLowerCase() + ", album: " + this.album + ")\n" );
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Mp3 other = (Mp3) o;
        if (this.compareTo(other) != 0)
        {
            return false;
        }
        if ( (this.singer != null)
             ? !this.singer.equals(other.singer)
             : other.singer != null )
        {
            return false;
        }
        return ( (this.album != null)
                 ? this.album.equals(other.album)
                 : other.album == null);

    }
}
