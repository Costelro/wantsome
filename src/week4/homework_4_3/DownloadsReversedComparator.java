package week4.homework_4_3;

import java.util.Comparator;

/**
 * Created by CostelRo on 15.06.2018.
 */


class DownloadsReversedComparator implements Comparator<MediaEntity>
{
    // Entities will be sorted in *reversed order* after their number of downloads,
    // the first having the largest number of downloads.

    public int compare(MediaEntity first, MediaEntity second)
    {
        return second.noOfDownloads - first.noOfDownloads;
    }
}
