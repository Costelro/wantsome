package week4.homework_4_3;

import java.util.ArrayList;

/**
 * Created by CostelRo on 15.06.2018.
 */

/*
 * ASSIGNMENT 4_3:
 * Write a class hierarchy for a virtual library that holds books, videos and music.
 * Note: Implement the entities, fields and methods according to the requirements file.
 */


public class Homework_4_3
{


    public static void main(String[] args)
    {
        // preparations for testing
        Library testLibrary = new Library();
        ArrayList<String> listOfTitles = generateTitles();


        // test 1
        for (int i=0; i < 15; i++)
        {
            testLibrary.addBook( testLibrary, listOfTitles.get(i),
                          "author"+i, "editura"+i );
        }

        for (int i=15; i < 20; i++)
        {
            boolean status = true;
            if (i%2 == 0) { status = false; }
            testLibrary.addVideo( testLibrary, listOfTitles.get(i),
                          (8+i) + ":" + (25+i), status ) ;
        }

        for (int i=20; i < 27; i++)
        {
            testLibrary.addMp3( testLibrary, listOfTitles.get(i),
                          "singer"+i, "album"+i );
        }

        System.out.printf( "Added media entities: %d\n***\n", testLibrary.getArchive().size() );

        // test 2
        String searchString  = "video";
        ArrayList<MediaEntity> searchResults = testLibrary.searchLibrary( searchString );
        testLibrary.displayListOfMediaEntities( searchResults, "Search result for: " + searchString );
        System.out.println("*** \n");


        // test 3
        testLibrary.getMediaEntity(26).setNoOfDownloads(100);
        testLibrary.getMediaEntity(0).setNoOfDownloads(200);
        System.out.println("*** \n");


        // test 4
        testLibrary.displayTop5();
        System.out.println("*** \n");


        // test 5
        testLibrary.getMediaEntity(26).updateNoOfDownloads(150);

        testLibrary.displayTop5();
        System.out.println("*** \n");
    }


    private static ArrayList<String> generateTitles()
    {
        ArrayList<String> results = new ArrayList<>(40);

        String[] part1 = {"The", "New", "Old", "Amazing"};
        String[] part2 = {"stuff", "musings", "enigmas"};
        String[] part3 = {"of the future", "with Ditai Lama", "about deepness"};

        for (String p1 : part1)
        {
            for (String p2 : part2)
            {
                for (String p3 : part3)
                {
                    results.add( p1 + " " + p2 + " " + p3 );
                }
            }
        }

        return results;
    }
}
